'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

//defuilt if cache not up
var keyJwt = "campusManagementConsorsium";

//sign with RSA SHA256
function generateToken(payload, expires, cb){
    //  var value = myCache.get( "keyJwtCache" );
    //  if ( value != undefined ){
    //      var keyJwt = value;
    //  }

    jwt.sign(payload, keyJwt, {expiresIn: expires}, (err, token) => {
      if(err){
        return cb(err, null);
      }
  
      return cb(null, token);
    });
  }
  
exports.loginAdmin = function(req, res) {
    
  //logging
  var activity = "loginAdmin";
  var requestId = req.body.header.requestId;
  var ip = req.body.header.ip;
  var device = req.body.header.device;
  var deviceVersion = req.body.header.deviceVersion;
  var location = req.body.header.location;
  var usernameheader = req.body.header.username;
  auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);


  var username = req.body.body.username;
  var password = req.body.body.password;
  password = base64Encode(password);

  connection.query('SELECT * FROM `admin` WHERE username = ? or email = ? or telephone = ?',
  [ username, username, username ],
  function (error, rows, fields){
    var usernameDB =  "null";
    var passwordDB = "null";
    var emailDB = "null";
    var telephoneDB = "null";

    if(rows.length >= 1){
      usernameDB = rows[0].username;
      emailDB = rows[0].email;
      telephoneDB = rows[0].telephone;
      passwordDB = rows[0].password;
    }
    
    if(error){
          console.log(error)
          logger.debug(error)
          loging(JSON.stringify(req.body), error, error, '500');
          response.httpResp(error.sqlMessage, "500", res)
      }else if(username != usernameDB){
           if(username != emailDB){
              if(username != telephoneDB){
                response.httpResp("username invalid", "202", res)
                loging(JSON.stringify(req.body), 'username invalid', 'username invalid', '202');           
              }else if(password != passwordDB){
                response.httpResp("password invalid", "203", res)
                loging(JSON.stringify(req.body), 'password invalid', 'password invalid', '203');
              }else if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("username or password invalid", "201", res)
                    loging(JSON.stringify(req.body), 'username or password invalid', 'username or password invalid', '201');
              }else{
                  generateToken({username: req.body.username}, 60 * 60, (err, token) => {
                      if(err){
                          response.httpResp("Generate Token Invalid", "203", res)
                          loging(JSON.stringify(req.body), 'Generate Token Invalid', 'Generate Token Invalid', '203'); 
                      }
                      response.ok(token, res)
                      loging(JSON.stringify(req.body), token, token, '200'); 
                    });
              }
          }else if(password != passwordDB){
            response.httpResp("password invalid", "203", res)
            loging(JSON.stringify(req.body), 'password invalid', 'password invalid', '203');
          }else if(rows == "" || rows == null || rows.length <= 0){
                response.httpResp("username or password invalid", "201", res)
                loging(JSON.stringify(req.body), 'password or username invalid', 'username or password invalid', '201');
          }else{
              generateToken({username: req.body.username}, 60 * 60, (err, token) => {
                  if(err){
                      response.httpResp("Generate Token Invalid", "203", res)
                      loging(JSON.stringify(req.body), 'Generate Token Invalid', 'Generate Token Invalid', '203'); 
                  }
                  response.ok(token, res)
                  loging(JSON.stringify(req.body), token, token, '200'); 
                });
          }
          
          

      }else if(password != passwordDB){
           response.httpResp("password invalid", "203", res)
           loging(JSON.stringify(req.body), 'password invalid', 'password invalid', '203');
      }else if(rows == "" || rows == null || rows.length <= 0){
           response.httpResp("username or password invalid", "201", res)
           loging(JSON.stringify(req.body), 'username or password invalid', 'username or password invalid', '201');
      }else{
          generateToken({username: req.body.username}, 60 * 60, (err, token) => {
              if(err){
                  response.httpResp("Generate Token Invalid", "203", res)
                  loging(JSON.stringify(req.body), 'Generate Token Invalid', 'Generate Token Invalid', '203'); 
              }
              response.ok(token, res)
              loging(JSON.stringify(req.body), token, token, '200'); 
            });
      }
  });
};

function auditTrail(username, activity, requestId, ip, device, deviceVersion, location){
  connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?)',
  [ username, activity, requestId, ip, device, deviceVersion, location],          
  function (error, rows, fields){
      if(error){
          console.log(error)
          logger.debug(error)
      }
  });
}

function loging(req, res, message, code){
  connection.query('INSERT INTO logging (date, req, res, message, code) values (Sysdate(),?,?,?,?)',
  [ req, res, message, code],          
  function (error, rows, fields){
      if(error){
          console.log(error)
          logger.debug(error)
      }
  });
}

function base64Encode (data){
  let buff = new Buffer(data);
  let base64data = buff.toString('base64');

  console.log('"' + data + '" converted to Base64 is "' + base64data + '"');
  return base64data;
}

function base64Decode (data){
  let buff = new Buffer(data, 'base64');
  let text = buff.toString('ascii');
  
  console.log('"' + data + '" converted from Base64 to ASCII is "' + text + '"');
  return text;
}