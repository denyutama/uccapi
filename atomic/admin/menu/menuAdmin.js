'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');

  exports.masterAdminAccess = function(req, res) {
    //logging
    var activity = "masterAdminAccess";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM masterAdminAccess',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "masterAdminAccess", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "masterAdminAccess", "User Information Not Found", '201');                    
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "masterAdminAccess", rows, '200');
                }
            });   
        }
      });
  };

  exports.giveUserAccessToMenu = function(req, res) {
    //logging
    var activity = "giveUserAccessToMenu";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var menuId = req.body.body.menuId;
    var status = req.body.body.status;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updateDate = req.body.body.updateDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var username = req.body.body.username;

  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO userAccess (accessMenuId, status, createdDate, expiredDate, updateDate, createdBy, updatedBy, username) values (?,?,?,?,?,?,?,?)',
            [ menuId, status, createdDate, expiredDate, updateDate, createdBy, updatedBy, username ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "giveUserAccessToMenu", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "giveUserAccessToMenu", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add User to access menu", res)
                    loging(JSON.stringify(req.body), "giveUserAccessToMenu", "Success add User to access menu", '200');                    
                }
            });   
        }
      });
  };
  
  exports.inquiryAdminMenu = function(req, res) {
    //logging
    var activity = "inquiryAdminMenu";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from uccapp.adminMenu where groupMenu = (SELECT accessId FROM uccapp.adminRole where username = ?)',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquiryAdminMenu", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquiryAdminMenu", "User Information Not Found", '201');                    
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquiryAdminMenu", rows, '200');
                }
            });   
        }
      });
  };

exports.inquiryadminSubMenu = function(req, res) {  //logging
    var activity = "inquiryadminSubMenu";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from adminSubMenu where idMenu = ?',
            [ idMenu ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquiryadminSubMenu", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquiryadminSubMenu", "User Information Not Found", '201');                    
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquiryadminSubMenu", rows, '200');
                }
            });   
        }
      });
  };

  exports.addMenuEnduser = function(req, res) {
    //logging
    var activity = "addMenuEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var menuTittle = req.body.body.menuTittle;
    var menuImage = req.body.body.menuImage;
    var note = req.body.body.note;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var status = req.body.body.status;
    var priority = req.body.body.priority;
    var groupMenu = "1";//req.body.body.groupMenu;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO menu (menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu) values (?,?,?,?,?,?,?,?)',
            [ menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "addMenuEnduser", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "addMenuEnduser", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add User menu", res)
                    loging(JSON.stringify(req.body), "addMenuEnduser", "Success add User to access menu", '200');                    
                }
            });   
        }
      });
  };

  exports.addSubMenuEnduser = function(req, res) {
    //logging
    var activity = "addSubMenuEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var submenuName = req.body.body.submenuName;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
    var note = req.body.body.note;
    var createdBy = req.body.body.createdBy;
    var idmenu = req.body.body.idmenu;
    var priority = req.body.body.priority;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (submenuName, createdDate, expiredDate, updateDate, updatedBy, status, note, createdBy, idmenu, priority) values (?,?,?,?,?,?,?,?,?)',
            [ submenuName, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy, idmenu, priority ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "addSubMenuEnduser", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "addSubMenuEnduser", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add User menu", res)
                    loging(JSON.stringify(req.body), "addSubMenuEnduser", "Success add User menu", '200');                    
                }
            });   
        }
      });
  };


  exports.addMenuAdmin = function(req, res) {
    //logging
    var activity = "addMenuAdmin";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    //var idMenu = req.body.body.idMenu;
    var menuTittle = req.body.body.menuTittle;
    var menuImage = req.body.body.menuImage;
    var note = req.body.body.note;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var status = req.body.body.status;
    var priority = req.body.body.priority;
    var IdAdminAccess = req.body.body.IdAdminAccess;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO adminMenu ( menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu) values (?,?,?,?,?,?,?,?)',
            [ menuTittle, menuImage, note, createdDate, expiredDate, status, priority, IdAdminAccess ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "addMenuAdmin", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "addMenuAdmin", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add menu admin", res)
                    loging(JSON.stringify(req.body), "addMenuAdmin", "Success add menu admin", '200');   
                }
            });   
        }
      });
  };

  
  exports.addSubMenuAdmin = function(req, res) {
    //logging
    var activity = "addSubMenuAdmin";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var submenuName = req.body.body.submenuName;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
    var note = req.body.body.note;
    var createdBy = req.body.body.createdBy;
    var priority = req.body.body.priority;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (idMenu, submenuName, createdDate, expiredDate, updateDate, updatedBy, status, note, createdBy, priority) values (?,?,?,?,?,?,?,?,?,?)',
            [ idMenu, submenuName, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy, priority ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "addSubMenuAdmin", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "addSubMenuAdmin", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add User menu", res)
                    loging(JSON.stringify(req.body), "addSubMenuAdmin", "Success add User menu", '200');                    
                }
            });   
        }
      });
  };

  exports.inquiryAccessMatrix = function(req, res) {
    //logging
    var activity = "inquiryAccessMatrix";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('SELECT * FROM uccapp.accessMatrix where idmasterAdminAccess = (SELECT accessId FROM uccapp.adminRole where username = ?)',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquiryAccessMatrix", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Matrix Access Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquiryAccessMatrix", "Matrix Access Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquiryAccessMatrix", rows, '200');
                }
            });   
        }
      });
  };

  exports.addAccessMatrix = function(req, res) {
    //logging
    var activity = "addAccessMatrix";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;

    var idmasterAdminAccess = req.body.body.idmasterAdminAccess;
    var Create = req.body.body.Create;
    var Read = req.body.body.Read;
    var Update = req.body.body.Update;
    var Delete = req.body.body.Delete;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO accessMatrix (idmasterAdminAccess, Create, Read, Update, Delete, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy) values (?,?,?,?,?,?,?,?,?,?,?)',
            [ idmasterAdminAccess, Create, Read, Update, Delete, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "addAccessMatrix", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "addAccessMatrix", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add", res)
                    loging(JSON.stringify(req.body), "addAccessMatrix", "Success add", '200');                    
                }
            });   
        }
      });
  };

  exports.updateAccessMatrix = function(req, res) {
    //logging
    var activity = "updateAccessMatrix";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;

    var idAccessMatrix = req.body.body.idAccessMatrix
    var idmasterAdminAccess = req.body.body.idmasterAdminAccess;
    var Create = req.body.body.Create;
    var Read = req.body.body.Read;
    var Update = req.body.body.Update;
    var Delete = req.body.body.Delete;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;


    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{    
            connection.query('UPDATE accessMatrix SET idmasterAdminAccess = ?, Create = ?, Read = ?, Update = ?, Delete = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ? where idAccessMatrix = ?',
            [ idmasterAdminAccess, Create, Read, Update, Delete, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy, idAccessMatrix ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), error, error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Update Success","200", res)
                }
            });
        }
    });            
};

exports.deleteAccessMatrix = function(req, res) {
    //logging
    var activity = "deleteAccessMatrix";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idAccessMatrix = req.body.body.idAccessMatrix;

    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{    
            connection.query('DELETE FROM uccapp.accessMatrix where idAccessMatrix = ?',
            [ idAccessMatrix ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), error, error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Delete Success","200", res)
                }
            });
        }
    });            
};


  exports.inquirylandingPage = function(req, res) {
    //logging
    var activity = "inquirylandingPage";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
            connection.query('SELECT * FROM uccapp.landingPage',
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPage", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPage", "Landing Page Information Not Found", '200');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPage", rows, '200');
                }
            });   
  };

  exports.inquirylandingPageHeader = function(req, res) {
    //logging
    var activity = "inquirylandingPageHeader";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var landingPageName = req.body.body.landingPageName;

            connection.query('SELECT * FROM uccapp.landingPageHeader where idLandingPage = (SELECT idLandingPage FROM uccapp.landingPage where name= ? ) and status = "active"',
            [ landingPageName ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPageHeader", error.sqlMessage, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageHeader", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPageHeader", rows, '200');
                }
            });   
  };

  exports.inquirylandingPageHeaderByIdAdminAccs = function(req, res) {
    //logging
    var activity = "inquirylandingPageHeaderByIdAdminAccs";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idmasterAdminAccess = req.body.body.idmasterAdminAccess;
    
            connection.query('SELECT * FROM uccapp.landingPageHeader where idmasterAdminAccess = ? and status = "active"',
            [ idmasterAdminAccess ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPageHeaderByIdAdminAccs", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageHeaderByIdAdminAccs", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPageHeaderByIdAdminAccs", rows, '200');
                }
            });   
  };

  exports.inquirylandingPageMenuByIdAdminAccs = function(req, res) {
    //logging
    var activity = "inquirylandingPageMenuByIdAdminAccs";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idmasterAdminAccess = req.body.body.idmasterAdminAccess;
    
            connection.query('SELECT * FROM uccapp.landingPageMenu where idmasterAdminAccess = ? and status = "active"',
            [ idmasterAdminAccess ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPageMenuByIdAdminAccs", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageMenuByIdAdminAccs", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPageMenuByIdAdminAccs", rows, '200');
                }
            });   
  };


  exports.inquirylandingPageMenu = function(req, res) {
    //logging
    var activity = "inquirylandingPageMenu";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var landingPageName = req.body.body.landingPageName;
    
            connection.query('SELECT * FROM uccapp.landingPageMenu where idLandingPage = (SELECT idLandingPage FROM uccapp.landingPage where name= ? ) and status = "active"',
            [ landingPageName ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPageMenu", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageMenu", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPageMenu", rows, '200');
                }
            });   
  };

  exports.inquirylandingPageGroupContentByIdAdminAccs = function(req, res) {
    //logging
    var activity = "inquirylandingPageGroupContentByIdAdminAccs";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idmasterAdminAccess = req.body.body.idmasterAdminAccess;
                //group
            connection.query('SELECT * FROM uccapp.landingPageGroupContent where idmasterAdminAccess = ?',
            [ idmasterAdminAccess ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPageGroupContentByIdAdminAccs", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageGroupContentByIdAdminAccs", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPageGroupContentByIdAdminAccs", rows, '200');
                }
            });   
  };


  exports.inquirylandingPageGroupContent = function(req, res) {
    //logging
    var activity = "inquirylandingPageGroupContent";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var landingPageName = req.body.body.landingPageName;
                //group
            connection.query('SELECT * FROM uccapp.landingPageGroupContent where idLandingPage = (SELECT idLandingPage FROM uccapp.landingPage where name = ? )',
            [ landingPageName ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "inquirylandingPageGroupContent", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageGroupContent", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows, res);
                    loging(JSON.stringify(req.body), "inquirylandingPageGroupContent", rows, '200');
                }
            });   
  };

  exports.inquirylandingPageContentByIdContent = function(req, res) {
    //logging
    var activity = "inquirylandingPageContentByIdContent";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idContent = req.body.body.idContent;
    
            connection.query('SELECT * FROM uccapp.landingPageContent where idlandingContent = ?',
            [ idContent ], 
            function (error2, rows2, fields){
                if(error2){
                    console.log(error2)
                    logger.debug(error2)
                    loging(JSON.stringify(req.body), "inquirylandingPageContentByIdContent", error2, '500');                    
                    response.httpResp(error2.sqlMessage, "500", res)
                }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageContentByIdContent", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows2, res)
                    loging(JSON.stringify(req.body), "inquirylandingPageContentByIdContent", rows2, '200');                    
                }
            })
  };

  exports.inquirylandingPageContent = function(req, res) {
    //logging
    var activity = "inquirylandingPageContent";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idGroupContent = req.body.body.idGroupContent;
    
            connection.query('SELECT * FROM uccapp.landingPageContent where idGroupContent = ?',
            [ idGroupContent ], 
            function (error2, rows2, fields){
                if(error2){
                    console.log(error2)
                    logger.debug(error2)
                    loging(JSON.stringify(req.body), "inquirylandingPageContent", error2, '500');                    
                    response.httpResp(error2.sqlMessage, "500", res)
                }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquirylandingPageContent", "Landing Page Information Not Found", '201');
                } else{
                    response.ok(rows2, res)                    
                    loging(JSON.stringify(req.body), "inquirylandingPageContent", rows2, '200');                    
                }
            })
  };

  exports.inquiryMasterTypeTemplate = function(req, res) {
    //logging
    var activity = "inquiryMasterTypeTemplate";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM uccapp.masterTypeTemplate',
            function (error2, rows2, fields){
                if(error2){
                    console.log(error2)
                    logger.debug(error2)
                    loging(JSON.stringify(req.body), "inquiryMasterTypeTemplate", error2, '500');                    
                    response.httpResp(error2.sqlMessage, "500", res)
                }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                    response.httpResp("Landing Page Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquiryMasterTypeTemplate", "Landing Page Information Not Found", '201');                    
                } else{
                    response.ok(rows2, res)                    
                    loging(JSON.stringify(req.body), "inquiryMasterTypeTemplate", rows2, '200');                    
                }
            })
        }
      });
  };

    function dataContent(idGroupContent){
        let contentFinal = '';
        connection.query('SELECT * FROM uccapp.landingPageContent where idGroupContent = ?',
        [ idGroupContent ], 
        function (error2, rows2, fields){
            if(error2){
                console.log(error2)
                logger.debug(error2)
                response.httpResp(error2.sqlMessage, "500", res)
            }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                response.httpResp("Landing Page Information Not Found", "201", res)
            } else{
                let content = '';
                for (const property in rows2) {
                    content += ',{"Name" : "'+rows2[property].landingPageContentName+'", "Note" : "'+rows2[property].landingPageContentNote+'", "Image" : "'+rows2[property].landingPageContentImage+'"}'; 
                }
                contentFinal = content.substr(1);
                contentFinal = "["+contentFinal+"]"; 
                //console.log(contentFinal);
                return contentFinal;                       
            }
        })
                    // let dataResFinal = '';
                    // let dataRes = '';
                    // let content = '';                    
                    // for (const property in rows) {    
                    // let idGroupContent = rows[property].idGroupContent;
                    // let GroupContentName = rows[property].GroupContentName;
                    // //content
                    // content = dataContent(idGroupContent);
                    // console.log(content);
                    // dataRes += ',{"groupContentName" : "'+GroupContentName+'", "content" : '+content+'}';
                    // dataResFinal = dataRes.substr(1);
                    // dataResFinal = "["+dataResFinal+"]";
                    // response.ok(dataResFinal, res)
                    // }
                    // //end loop        
    }


    exports.addMasterTypeTemplate = function(req, res) {
        //logging
        var activity = "addMasterTypeTemplate";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var typeTemplate = req.body.body.typeTemplate;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO masterTypeTemplate (typeTemplate, createdDate, expiredDate, updatedDate, updatedBy, createdBy) values (?,?,?,?,?,?)',
                [ typeTemplate, createdDate, expiredDate, updatedDate, updatedBy, createdBy ],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "User Information Not Found", "User Information Not Found", '201');                        
                    } else{
                        response.ok("Success add", res)
                    }
                });   
            }
          });
      };

      exports.updateMasterTypeTemplate = function(req, res) {
        //logging
        var activity = "updateMasterTypeTemplate";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idTypeTemplate = req.body.body.idTypeTemplate;
        var typeTemplate = req.body.body.typeTemplate;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE masterTypeTemplate SET typeTemplate = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ? where idTypeTemplate = ?',
                [ typeTemplate, createdDate, expiredDate, updatedDate, updatedBy, createdBy, idTypeTemplate ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "updateMasterTypeTemplate", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };

    exports.deleteMasterTypeTemplate = function(req, res) {
        //logging
        var activity = "deleteMasterTypeTemplate";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idTypeTemplate = req.body.body.idTypeTemplate;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.masterTypeTemplate where idTypeTemplate = ?',
                [ idTypeTemplate ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "deleteMasterTypeTemplate", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };

      exports.addLandingPage = function(req, res) {
        //logging
        var activity = "addLandingPage";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var name = req.body.body.name;
        var image = req.body.body.image;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var status = req.body.body.status;
        var note = req.body.body.note;
      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO landingPage (name, image, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy) values (?,?,?,?,?,?,?,?,?)',
                [ name, image, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "User Information Not Found", "User Information Not Found", '201');                        
                    } else{
                        response.ok("Success add", res)
                    }
                });   
            }
          });
      };

      exports.updateLandingPage = function(req, res) {
        //logging
        var activity = "updateLandingPage";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idLandingPage = req.body.body.idLandingPage;
        var name = req.body.body.name;
        var image = req.body.body.image;
        var note = req.body.body.note;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE landingPage SET name = ?, image = ?, note = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ? where idLandingPage = ?',
                [ name, image, note, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, idLandingPage ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "updateLandingPage", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };

    exports.deleteLandingPage = function(req, res) {
        //logging
        var activity = "deleteLandingPage";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idLandingPage = req.body.body.idLandingPage;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.landingPage where idLandingPage = ?',
                [ idLandingPage ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "deleteLandingPage", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };

    exports.addLandingPageMenu = function(req, res) {
        //logging
        var activity = "addLandingPageMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        

        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var landingPageMenuName = req.body.body.landingPageMenuName;
        var landingPageMenuNote = req.body.body.landingPageMenuNote;
        var landingPageMenuIcon = req.body.body.landingPageMenuIcon;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var priority = req.body.body.priority;
        var idLandingPage = req.body.body.idLandingPage;

      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO landingPageMenu (landingPageMenuName, landingPageMenuNote, landingPageMenuIcon, createdDate, expiredDate, updatedDate, updatedBy, status, priority, createdBy, idLandingPage) values (?,?,?,?,?,?,?,?,?,?,?)',
                [ landingPageMenuName, landingPageMenuNote, landingPageMenuIcon, createdDate, expiredDate, updatedDate, updatedBy, status, priority, createdBy, idLandingPage],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "User Information Not Found", "User Information Not Found", '201');                        
                    } else{
                        response.ok("Success add", res)
                    }
                });   
            }
          });
      };

      exports.updateLandingPageMenu = function(req, res) {
        //logging
        var activity = "updateLandingPageMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idlandingMenu = req.body.body.idlandingMenu;
        var landingPageMenuName = req.body.body.landingPageMenuName;
        var landingPageMenuNote = req.body.body.landingPageMenuNote;
        var landingPageMenuIcon = req.body.body.landingPageMenuIcon;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var priority = req.body.body.priority;
        var idLandingPage = req.body.body.idLandingPage;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE landingPageMenu SET landingPageMenuName = ?, landingPageMenuNote = ?, landingPageMenuIcon = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ?, priority = ?, idLandingPage = ? where idlandingMenu = ?',
                [ landingPageMenuName, landingPageMenuNote, landingPageMenuIcon, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, priority, idLandingPage, idlandingMenu ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "updateLandingPageMenu", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };

    exports.deleteLandingPageMenu = function(req, res) {
        //logging
        var activity = "deleteLandingPageMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idlandingMenu = req.body.body.idlandingMenu;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.landingPageMenu where idlandingMenu = ?',
                [ idlandingMenu ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "updateLandingPageMenu", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "updateLandingPageMenu", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };

    exports.addLandingPageHeader = function(req, res) {
        //logging
        var activity = "addLandingPageHeader";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        

        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var landingPageHeaderName = req.body.body.landingPageHeaderName;
        var landingPageHeaderNote = req.body.body.landingPageHeaderNote;
        var landingPageHeaderImage = req.body.body.landingPageHeaderImage;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var idLandingPage = req.body.body.idLandingPage;

      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO landingPageHeader (landingPageHeaderName, landingPageHeaderNote, landingPageHeaderImage, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy, idLandingPage) values (?,?,?,?,?,?,?,?,?,?)',
                [ landingPageHeaderName, landingPageHeaderNote, landingPageHeaderImage, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy, idLandingPage],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), error, error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "addLandingPageHeader", "User Information Not Found", '201');                        
                    } else{
                        response.ok("Success add", res)
                    }
                });   
            }
          });
      };

      exports.updateLandingPageHeader = function(req, res) {
        //logging
        var activity = "updateLandingPageHeader";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idlandingPageHeader = req.body.body.idlandingPageHeader;
        var landingPageHeaderName = req.body.body.landingPageHeaderName;
        var landingPageHeaderNote = req.body.body.landingPageHeaderNote;
        var landingPageHeaderImage = req.body.body.landingPageHeaderImage;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var idLandingPage = req.body.body.idLandingPage;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE landingPageHeader SET landingPageHeaderName = ?, landingPageHeaderNote = ?, landingPageHeaderImage = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ?, idLandingPage = ? where idlandingPageHeader = ?',
                [ landingPageHeaderName, landingPageHeaderNote, landingPageHeaderImage, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, idLandingPage, idlandingPageHeader ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "updateLandingPageHeader", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "updateLandingPageHeader", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };

    exports.deleteLandingPageHeader = function(req, res) {
        //logging
        var activity = "deleteLandingPageHeader";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idlandingPageHeader = req.body.body.idlandingPageHeader;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.landingPageHeader where idlandingPageHeader = ?',
                [ idlandingPageHeader ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "deleteLandingPageHeader", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "deleteLandingPageHeader", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };

    exports.addLandingPageContent = function(req, res) {
        //logging
        var activity = "addLandingPageContent";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        

        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var landingPageContentName = req.body.body.landingPageContentName;
        var landingPageContentNote = req.body.body.landingPageContentNote;
        var landingPageContentImage = req.body.body.landingPageContentImage;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var priority = req.body.body.priority;
        var idGroupContent = req.body.body.idGroupContent;

      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO landingPageContent (landingPageContentName, landingPageContentNote, landingPageContentImage, createdDate, expiredDate, updatedDate, updatedBy, status, priority, createdBy, idGroupContent) values (?,?,?,?,?,?,?,?,?,?,?)',
                [ landingPageContentName, landingPageContentNote, landingPageContentImage, createdDate, expiredDate, updatedDate, updatedBy, status, priority, createdBy, idGroupContent],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "addLandingPageContent", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "addLandingPageContent", "User Information Not Found", '201');                        
                    } else{
                        response.ok("Success add", res)
                    }
                });   
            }
          });
      };

      exports.updateLandingPageContent = function(req, res) {
        //logging
        var activity = "updateLandingPageContent";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idlandingContent = req.body.body.idlandingContent;
        var landingPageContentName = req.body.body.landingPageContentName;
        var landingPageContentNote = req.body.body.landingPageContentNote;
        var landingPageContentImage = req.body.body.landingPageContentImage;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var priority = req.body.body.priority;
        var idGroupContent = req.body.body.idGroupContent;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE landingPageContent SET landingPageContentName = ?, landingPageContentNote = ?, landingPageContentImage = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ?, priority = ?, idGroupContent = ? where idlandingContent = ?',
                [ landingPageContentName, landingPageContentNote, landingPageContentImage, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, priority, idGroupContent, idlandingContent ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "updateLandingPageContent", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                        loging(JSON.stringify(req.body), "updateLandingPageContent", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };

    exports.deleteLandingPageContent = function(req, res) {
        //logging
        var activity = "deleteLandingPageContent";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idlandingContent = req.body.body.idlandingContent;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.landingPageContent where idlandingContent = ?',
                [ idlandingContent ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "deleteLandingPageContent", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "deleteLandingPageContent", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };

    exports.addLandingPageGroupContent = function(req, res) {
        //logging
        var activity = "addLandingPageGroupContent";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
            
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idLandingPage = req.body.body.idLandingPage;
        var GroupContentName = req.body.body.GroupContentName;
        var idTypeTemplate = req.body.body.idTypeTemplate;
        var status = req.body.body.status;
        var idmasterAdminAccess = req.body.body.idmasterAdminAccess;        
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;

      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO landingPageGroupContent (idLandingPage, GroupContentName, idTypeTemplate, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy, idmasterAdminAccess) values (?,?,?,?,?,?,?,?,?,?)',
                [ idLandingPage, GroupContentName, idTypeTemplate, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy, idmasterAdminAccess],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "addLandingPageGroupContent", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "addLandingPageGroupContent", "User Information Not Found", '201');                        
                    } else{
                        loging(JSON.stringify(req.body), "addLandingPageGroupContent", "success add", '200');                        
                        response.ok("Success add", res)
                    }
                });   
            }
          });
      };

      exports.updateLandingPageGroupContent = function(req, res) {
        //logging
        var activity = "updateLandingPageGroupContent";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idGroupContent = req.body.body.idGroupContent;
        var idLandingPage = req.body.body.idLandingPage;
        var GroupContentName = req.body.body.GroupContentName;
        var idTypeTemplate = req.body.body.idTypeTemplate;
        var status = req.body.body.status;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE landingPageGroupContent SET idLandingPage = ?, GroupContentName = ?, idTypeTemplate = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ? where idGroupContent = ?',
                [ idLandingPage, GroupContentName, idTypeTemplate, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, idGroupContent ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "updateLandingPageGroupContent", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                        loging(JSON.stringify(req.body), "updateLandingPageGroupContent", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };

    exports.deleteLandingPageGroupContent = function(req, res) {
        //logging
        var activity = "deleteLandingPageGroupContent";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idGroupContent = req.body.body.idGroupContent;

        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.landingPageGroupContent where idGroupContent = ?',
                [ idGroupContent ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "deleteLandingPageGroupContent", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    loging(JSON.stringify(req.body), "deleteLandingPageGroupContent", "User Information Not Found", '201');
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };

    exports.inquiryChildSubMenu = function(req, res) {
        //logging
        var activity = "inquiryChildSubMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idsubmenu = req.body.body.idsubmenu;
        
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
    
                connection.query('SELECT * FROM uccapp.childSubMenu where idsubmenu = ?',
                [ idsubmenu ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "inquiryChildSubMenu", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("ChildSubMenu Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "inquiryChildSubMenu", "ChildSubMenu Information Not Found", '201');
                    } else{
                        response.ok(rows, res);
                        loging(JSON.stringify(req.body), "inquiryChildSubMenu", rows, '200');
                    }
                });   
            }
          });
      };
    
      exports.addChildSubMenu = function(req, res) {
        //logging
        var activity = "addChildSubMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
    
        var idsubmenu = req.body.body.idsubmenu;
        var childMenuName = req.body.body.childMenuName;
        var image = req.body.body.image;
        var priority = req.body.body.priority;
        var note = req.body.body.note;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var status = req.body.body.status;
      
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{
                connection.query('INSERT INTO childSubMenu (idsubmenu, childMenuName, image, priority, note, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy) values (?,?,?,?,?,?,?,?,?,?,?)',
                [ idsubmenu, childMenuName, image, priority, note, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy],          
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "addChildSubMenu", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == "" || rows == null || rows.length <= 0){
                        response.httpResp("User Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "addChildSubMenu", "ChildSubMenu Information Not Found", '201');                    
                    } else{
                        response.ok("Success add", res)
                        loging(JSON.stringify(req.body), "addChildSubMenu", "Success add", '200');                    
                    }
                });   
            }
          });
      };
    
      exports.updateChildSubMenu = function(req, res) {
        //logging
        var activity = "updateChildSubMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
    
        var idsubmenu = req.body.body.idsubmenu;
        var childMenuName = req.body.body.childMenuName;
        var image = req.body.body.image;
        var priority = req.body.body.priority;
        var note = req.body.body.note;
        var createdDate = req.body.body.createdDate;
        var expiredDate = req.body.body.expiredDate;
        var updatedDate = req.body.body.updatedDate;
        var createdBy = req.body.body.createdBy;
        var updatedBy = req.body.body.updatedBy;
        var status = req.body.body.status;
        var idChildSubMenu = req.body.body.idChildSubMenu;
    
    
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('UPDATE childSubMenu SET idsubmenu = ?, childMenuName = ?, image = ?, priority = ?, note = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ? where idChildSubMenu = ?',
                [ idsubmenu, childMenuName, image, priority, note, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, idChildSubMenu ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "updateChildSubMenu", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    } else{
                        response.httpResp("Update Success","200", res)
                    }
                });
            }
        });            
    };
    
    exports.deleteChildSubMenu = function(req, res) {
        //logging
        var activity = "deleteChildSubMenu";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
    
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idChildSubMenu = req.body.body.idChildSubMenu;
    
        jwt.verify(token, jwtKey, function(err, decoded) {
            console.log(decoded) // content in jwt
            if(err){
                response.httpResp(err, "599", res)
            }else{    
                connection.query('DELETE FROM uccapp.childSubMenu where idChildSubMenu = ?',
                [ idChildSubMenu ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        loging(JSON.stringify(req.body), "deleteChildSubMenu", error, '500');
                        response.httpResp(error.sqlMessage, "500", res)
                    }if(rows == null || rows.length <= 0){
                        response.httpResp("Data Not Found", "200", res)
                    } else{
                        response.httpResp("Delete Success","200", res)
                    }
                });
            }
        });            
    };
    
    exports.inquiryProvince = function(req, res) {
        //logging
        var activity = "inquiryProvince";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var searchName = req.body.body.searchName;
        
        var query = "SELECT * FROM uccapp.provinces";
        if (searchName != undefined || searchName != "undefined" || searchName != "" || searchName != null){
            query = query+" where name like '%"+searchName+"%'";
        }
        console.log(query);
                connection.query(query,
                function (error2, rows2, fields){
                    if(error2){
                        console.log(error2)
                        logger.debug(error2)
                        loging(JSON.stringify(req.body), "inquiryProvince", error2, '500');                    
                        response.httpResp(error2.sqlMessage, "500", res)
                    }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                        response.httpResp("inquiryProvince Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "inquiryProvince", "inquiryProvince Information Not Found", '201');
                    } else{
                        response.ok(rows2, res)
                        loging(JSON.stringify(req.body), "inquiryProvince", JSON.stringify(rows2), '200');                    
                    }
                })
      };

      exports.inquiryRegencies = function(req, res) {
        //logging
        var activity = "inquiryRegencies";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idProvince = req.body.body.idProvince;
        var searchName = req.body.body.searchName;

        var query = "SELECT * FROM uccapp.regencies where province_id = ?";
        if (searchName != undefined || searchName != "undefined" || searchName != "" || searchName != null){
            query = query+" and name like '%"+searchName+"%'";
        }
                connection.query(query,
                [ idProvince ], 
                function (error2, rows2, fields){
                    if(error2){
                        console.log(error2)
                        logger.debug(error2)
                        loging(JSON.stringify(req.body), "inquiryRegencies", error2, '500');                    
                        response.httpResp(error2.sqlMessage, "500", res)
                    }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                        response.httpResp("Regencies Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "inquiryRegencies", "inquiryRegencies Information Not Found", '201');
                    } else{
                        response.ok(rows2, res)
                        loging(JSON.stringify(req.body), "inquiryRegencies", rows2, '200');                    
                    }
                })
      };

      exports.inquiryDistricts = function(req, res) {
        //logging
        var activity = "inquiryPrinquiryDistrictsovince";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idRegency = req.body.body.idRegency;
        var searchName = req.body.body.searchName;

        var query = "SELECT * FROM uccapp.districts where regency_id = ?";
        if (searchName != undefined || searchName != "undefined" || searchName != "" || searchName != null){
            query = query+" and name like '%"+searchName+"%'";
        }
                connection.query(query,
                [ idRegency ], 
                function (error2, rows2, fields){
                    if(error2){
                        console.log(error2)
                        logger.debug(error2)
                        loging(JSON.stringify(req.body), "inquiryDistricts", error2, '500');                    
                        response.httpResp(error2.sqlMessage, "500", res)
                    }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                        response.httpResp("Districts Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "inquiryDistricts", "inquiryDistricts Information Not Found", '201');
                    } else{
                        response.ok(rows2, res)
                        loging(JSON.stringify(req.body), "inquiryDistricts", rows2, '200');                    
                    }
                })
      };

    exports.inquiryVillages = function(req, res) {
        //logging
        var activity = "inquiryVillages";
        var requestId = req.body.header.requestId;
        var ip = req.body.header.ip;
        var device = req.body.header.device;
        var deviceVersion = req.body.header.deviceVersion;
        var location = req.body.header.location;
        var usernameheader = req.body.header.username;
        auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
        
        var token = req.body.header.token;
        var jwtKey = req.body.header.jwtKey;
        var idDistrict = req.body.body.idDistrict;
        var searchName = req.body.body.searchName;

        var query = "SELECT * FROM uccapp.villages where district_id = ?";
        if (searchName != undefined || searchName != "undefined" || searchName != "" || searchName != null){
            query = query+" and name like '%"+searchName+"%'";
        }
                connection.query(query,
                [ idDistrict ], 
                function (error2, rows2, fields){
                    if(error2){
                        console.log(error2)
                        logger.debug(error2)
                        loging(JSON.stringify(req.body), "inquiryVillages", error2, '500');                    
                        response.httpResp(error2.sqlMessage, "500", res)
                    }if(rows2 == "" || rows2 == null || rows2.length <= 0){
                        response.httpResp("Landing Page Information Not Found", "201", res)
                        loging(JSON.stringify(req.body), "inquiryVillages", "inquiryVillages Information Not Found", '201');
                    } else{
                        response.ok(rows2, res)
                        loging(JSON.stringify(req.body), "inquiryVillages", rows2, '200');                    
                    }
                })
      };


    function loging(req, res, message, code){
        connection.query('INSERT INTO logging (date, req, res, message, code) values (Sysdate(),?,?,?,?)',
        [ req, res, message, code],          
        function (error, rows, fields){
            if(error){
                console.log(error)
                logger.debug(error)
            }
        });
      }
      
    function auditTrail(username, activity, requestId, ip, device, deviceVersion, location){
        connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?)',
        [ username, activity, requestId, ip, device, deviceVersion, location],          
        function (error, rows, fields){
            if(error){
                console.log(error)
                logger.debug(error)
            }
        });
    }