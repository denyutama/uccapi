'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');

  
exports.inquiryAdmin = function(req, res) {
//logging
var activity = "inquiryAdmin";
var requestId = req.body.header.requestId;
var ip = req.body.header.ip;
var device = req.body.header.device;
var deviceVersion = req.body.header.deviceVersion;
var location = req.body.header.location;
var usernameheader = req.body.header.username;
auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);


  var token = req.body.header.token;
  var jwtKey = req.body.header.jwtKey;
  var username = req.body.body.username;

  jwt.verify(token, jwtKey, function(err, decoded) {
      console.log(decoded) // content in jwt
      if(err){
          response.httpResp(err, "599", res)
          loging(JSON.stringify(req.body), "inquiryAdmin", err, '599');
      }else{
          connection.query('SELECT * FROM `admin` WHERE username = ?',
          [ username ], 
          function (error, rows, fields){
              if(error){
                  console.log(error)
                  logger.debug(error)
                  response.httpResp(error.sqlMessage, "500", res)
                  loging(JSON.stringify(req.body), "inquiryAdmin", error.sqlMessage, '500');
              }if(rows == "" || rows == null || rows.length <= 0){
                  response.httpResp("User Information Not Found", "201", res)
                  loging(JSON.stringify(req.body), "inquiryAdmin", "User Information Not Found", '201');
              } else{
                  response.ok(rows, res)
                  loging(JSON.stringify(req.body), "inquiryAdmin", rows, '200');
              }
          });   
      }
    });
};

exports.selfUpdateAdmin = function(req, res) {
    //logging
    var activity = "selfUpdateAdmin";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);
    var name = req.body.body.name;
    var email = req.body.body.email;
    var telephone = req.body.body.telephone;

    connection.query('UPDATE admin SET password = ?, name = ?, email = ?, telephone = ? where username = ?',
    [ password, name, email, telephone,  username ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "selfUpdateAdmin", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "selfUpdateAdmin", "Data Not Found", '201');
        } else{
            response.httpResp("Update Success","200", res)
            loging(JSON.stringify(req.body), "selfUpdateAdmin", "Update Success", '200');
        }
    });            
};


exports.registerAdmin = function(req, res) {
    //logging
    var activity = "registerAdmin";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);
    var name = req.body.body.name;
    var email = req.body.body.email;
    var status = "active";//req.body.body.status;
    var createdDate = Date.now();//req.body.body.createdDate;
    var expiredDate = Date.now()+1;//req.body.body.expiredDate;
    var telephone = req.body.body.telephone;
    var accessId = req.body.body.accessId;

    connection.query('SELECT * FROM `admin` WHERE username = ? or email = ? or telephone = ?',
    [ username, email, telephone ],
    function (error, rows, fields){
      var usernameDB =  "null";
      var emailDB = "null";
      var telephoneDB = "null";
  
      if(rows.length >= 1){
        usernameDB = rows[0].username;
        emailDB = rows[0].email;
        telephoneDB = rows[0].telephone;
      }
      
      if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res);
            loging(JSON.stringify(req.body), "registerAdmin", error.sqlMessage, '500');
        }else if(username == usernameDB){
            response.httpResp("Sorry "+name+",username already registered,please use another username", "203", res);
            loging(JSON.stringify(req.body), "registerAdmin", "Sorry "+name+",username already registered,please use another username", '203');
        }else if(email == emailDB){
             response.httpResp("Sorry "+name+",email already registered,please use another email", "203", res);
             loging(JSON.stringify(req.body), "registerAdmin", "Sorry "+name+",email already registered,please use another username", '203');
        }else if(telephone == telephoneDB){
            response.httpResp("Sorry "+name+",telephone already registered,please use another telephone", "203", res);
            loging(JSON.stringify(req.body), "registerAdmin", "Sorry "+name+",telephone already registered,please use another username", '203');
        }else{
            //----
            connection.query('INSERT INTO admin (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
            [ username, password, name, email, status,  telephone ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res);
                    loging(JSON.stringify(req.body), "registerAdmin", error.sqlMessage, '500');
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "201", res);
                    loging(JSON.stringify(req.body), "registerAdmin", "Data Not Found", '201');
                } else{
                    connection.query('INSERT INTO adminRole (accessId, status, createdDate, expiredDate, username) values (?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
                    [ accessId, status, username ],
                    function (error2, rows2, fields2){
                        if(error2){
                            console.log(error2)
                            logger.debug(error2)
                            response.httpResp(error2.sqlMessage, "500", res)
                            loging(JSON.stringify(req.body), "registerAdmin", error2.sqlMessage, '500');
                        }if(rows2 == null || rows2.length <= 0){
                            response.httpResp("Data Not Found", "201", res)
                            loging(JSON.stringify(req.body), "registerAdmin", "Data Not Found", '201');
                        } else{
                            response.httpResp("Register Success","200", res)
                            loging(JSON.stringify(req.body), "registerAdmin", "Register Success", '200');
                        }
                    });
                }
            }); 
            //---
        }
    });
  };
  
  function insertAdmin(username, password, name, email, status,  telephone, accessId){
    connection.query('INSERT INTO admin (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
    [ username, password, name, email, status,  telephone ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res);
            loging(JSON.stringify(req.body), "insertAdmin", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res);
            loging(JSON.stringify(req.body), "insertAdmin", "Data Not Found", '201');
        } else{
            connection.query('INSERT INTO adminRole (accessId, status, createdDate, expiredDate, username) values (?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
            [ accessId, status, username ],
            function (error2, rows2, fields2){
                if(error2){
                    console.log(error2)
                    logger.debug(error2)
                    response.httpResp(error2.sqlMessage, "500", res);
                    loging(JSON.stringify(req.body), "insertAdmin", error2.sqlMessage, '500');
                }if(rows2 == null || rows2.length <= 0){
                    response.httpResp("Data Not Found", "201", res);
                    loging(JSON.stringify(req.body), "insertAdmin", "Data Not Found", '201');
                } else{
                    response.httpResp("Register Success","200", res);
                    loging(JSON.stringify(req.body), "insertAdmin", "Register Success", '200');
                }
            });
        }
    });            
  }

exports.registerAdmin2 = function(req, res) {
    //logging
    var activity = "registerAdmin";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
        var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);
    var name = req.body.body.name;
    var email = req.body.body.email;
    var status = "active";//req.body.body.status;
    var createdDate = Date.now();//req.body.body.createdDate;
    var expiredDate = Date.now()+1;//req.body.body.expiredDate;
    var telephone = req.body.body.telephone;
    var accessId = req.body.body.accessId;

    

    connection.query('INSERT INTO admin (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
    [ username, password, name, email, status,  telephone ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "200", res)
        } else{
            connection.query('INSERT INTO adminRole (accessId, status, createdDate, expiredDate, username) values (?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
            [ accessId, status, username ],
            function (error2, rows2, fields2){
                if(error2){
                    console.log(error2)
                    logger.debug(error2)
                    response.httpResp(error2.sqlMessage, "500", res)
                }if(rows2 == null || rows2.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Register Success","200", res)
                }
            });
        }
    });            
};

exports.registerUccamp = function(req, res) {
    //logging
    var activity = "registerUccamp";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var campusName = req.body.body.campusName;
    var campusAccreditation = req.body.body.campusAccreditation;
    var address = req.body.body.address;
    var district = req.body.body.district;
    var subdistrict = req.body.body.subdistrict;
    var city = req.body.body.city;
    var province = req.body.body.province;
    var country = req.body.body.country;
    var docOpeningApprovalLetterCampus = req.body.body.docOpeningApprovalLetterCampus;
    var docNotaryDeedofEstablishment = req.body.body.docNotaryDeedofEstablishment;
    var docRatificationCampus = req.body.body.docRatificationCampus;
    var campusFoto = req.body.body.campusFoto;

    connection.query('INSERT INTO registrationCampus (username, registrationDate, campusName, campusAccreditation, address, district,  subdistrict,  city,  province,  country,  docOpeningApprovalLetterCampus,  docNotaryDeedofEstablishment,  docRatificationCampus,  campusFoto) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, campusName, campusAccreditation, address, district,  subdistrict,  city,  province,  country,  docOpeningApprovalLetterCampus,  docNotaryDeedofEstablishment,  docRatificationCampus,  campusFoto ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUccamp", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUccamp", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUccamp", "Register Success", '200');
        }
    });            
};

exports.registerUcjob = function(req, res) {
    //logging
    var activity = "registerUcjob";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var companyName = req.body.body.companyName;
    var companyDescription = req.body.body.companyDescription;
    var ownerName = req.body.body.ownerName;
    var kindOfCompany = req.body.body.kindOfCompany;
    var address = req.body.body.address;
    var district = req.body.body.district;
    var subdistrict = req.body.body.subdistrict;
    var city = req.body.body.city;
    var province = req.body.body.province;
    var country = req.body.body.country;
    var companyFoto = req.body.body.companyFoto;
    var ownerFoto = req.body.body.ownerFoto;

    connection.query('INSERT INTO registrationJob (username, registrationDate, companyName, companyDescription, ownerName, kindOfCompany, address, district,  subdistrict,  city,  province,  country,  companyFoto,  ownerFoto) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, companyName, companyDescription, ownerName, kindOfCompany, address, district,  subdistrict,  city,  province,  country,  companyFoto,  ownerFoto ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUcjob", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUcjob", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUcjob", "Register Success", '200');
        }
    });            
};

exports.registerUcshop = function(req, res) {
    //logging
    var activity = "registerUcshop";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var shopName = req.body.body.shopName;
    var shopDescription = req.body.body.shopDescription;
    var ownerName = req.body.body.ownerName;
    var kindOfShop = req.body.body.kindOfShop;
    var address = req.body.body.address;
    var district = req.body.body.district;
    var subdistrict = req.body.body.subdistrict;
    var city = req.body.body.city;
    var province = req.body.body.province;
    var country = req.body.body.country;
    var shopFoto = req.body.body.shopFoto;
    var ownerFoto = req.body.body.ownerFoto;

    connection.query('INSERT INTO registrationShop (username, registrationDate, shopName, shopDescription, ownerName, kindOfShop, address, district,  subdistrict,  city,  province,  country,  shopFoto,  ownerFoto) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, shopName, shopDescription, ownerName, kindOfShop, address, district,  subdistrict,  city,  province,  country,  shopFoto,  ownerFoto ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUcshop", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUcshop", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUcshop", "Register Success", '200');
        }
    });            
};


exports.registerUcteach = function(req, res) {
    //logging
    var activity = "registerUcteach";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var employeeNo = req.body.body.employeeNo;
    var nameWithTittle = req.body.body.nameWithTittle;
    var birthDate = req.body.body.birthDate;
    var placeOfBirth = req.body.body.placeOfBirth;
    var address = req.body.body.address;
    var foto = req.body.body.foto;
    var citizenship = req.body.body.citizenship;
    var religion = req.body.body.religion;
    var lastEducation = req.body.body.lastEducation;
    var yearsOfTeaching = req.body.body.yearsOfTeaching;
    var gender = req.body.body.gender;
    var docCV = req.body.body.docCV;
    var docCertificate = req.body.body.docCertificate;
    var docTranscripts = req.body.body.docTranscripts;
    var docTeachingExperience = req.body.body.docTeachingExperience;

    connection.query('INSERT INTO registrationTeach (username, registrationDate, employeeNo, nameWithTittle, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation,  yearsOfTeaching, gender,  docCV,  docCertificate,  docTranscripts,  docTeachingExperience) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, employeeNo, nameWithTittle, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation,  yearsOfTeaching, gender,  docCV,  docCertificate,  docTranscripts,  docTeachingExperience], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUcteach", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUcteach", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUcteach", "Register Success", '200');
        }
    });            
};

exports.registerUcbiz = function(req, res) {
    //logging
    var activity = "registerUcbiz";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var nameWithTittle = req.body.body.nameWithTittle;
    var birthDate = req.body.body.birthDate;
    var placeOfBirth = req.body.body.placeOfBirth;
    var address = req.body.body.address;
    var foto = req.body.body.foto;
    var citizenship = req.body.body.citizenship;
    var religion = req.body.body.religion;
    var lastEducation = req.body.body.lastEducation;
    var gender = req.body.body.gender;
    var docCV = req.body.body.docCV;
    var docCertificate = req.body.body.docCertificate;
    var docTranscripts = req.body.body.docTranscripts;

    connection.query('INSERT INTO registrationBiz (username, registrationDate, nameWithTittle, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation, gender,  docCV,  docCertificate,  docTranscripts) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, nameWithTittle, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation, gender,  docCV,  docCertificate,  docTranscripts], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUcbiz", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUcbiz", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUcbiz", "Register Success", '200');
        }
    });            
};

exports.registerUclearn = function(req, res) {
    //logging
    var activity = "registerUclearn";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var campusOrCompany = req.body.body.campusOrCompany;
    var nameWithTittle = req.body.body.nameWithTittle;
    var birthDate = req.body.body.birthDate;
    var placeOfBirth = req.body.body.placeOfBirth;
    var address = req.body.body.address;
    var foto = req.body.body.foto;
    var citizenship = req.body.body.citizenship;
    var religion = req.body.body.religion;
    var lastEducation = req.body.body.lastEducation;
    var gender = req.body.body.gender;
    var docCV = req.body.body.docCV;
    var docCertificate = req.body.body.docCertificate;
    var docTranscripts = req.body.body.docTranscripts;

    connection.query('INSERT INTO registrationLearn (username, registrationDate, campusOrCompany, nameWithTittle, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation, gender,  docCV,  docCertificate,  docTranscripts) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, campusOrCompany, nameWithTittle, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation, gender,  docCV,  docCertificate,  docTranscripts], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUclearn", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUclearn", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUclearn", "Register Success", '200');
        }
    });            
};


exports.registerUcalumni = function(req, res) {
    //logging
    var activity = "registerUcalumni";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var nameWithTittle = req.body.body.nameWithTittle;
    var campusName = req.body.body.campusName
    var collegeStudentNo = req.body.body.collegeStudentNo
    var birthDate = req.body.body.birthDate;
    var placeOfBirth = req.body.body.placeOfBirth;
    var address = req.body.body.address;
    var foto = req.body.body.foto;
    var citizenship = req.body.body.citizenship;
    var religion = req.body.body.religion;
    var lastEducation = req.body.body.lastEducation;
    var yearsOfGraduation = req.body.body.yearsOfGraduation;
    var yearsOfEntry = req.body.body.yearsOfEntry;
    var gender = req.body.body.gender;
    var docCV = req.body.body.docCV;
    var docCertificate = req.body.body.docCertificate;
    var docTranscripts = req.body.body.docTranscripts;

    connection.query('INSERT INTO registrationAlumni (username, registrationDate, campusName, nameWithTittle, collegeStudentNo, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation,  yearsOfGraduation, yearsOfEntry, gender,  docCV,  docCertificate,  docTranscripts) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
    [ username, campusName, nameWithTittle, collegeStudentNo, birthDate, placeOfBirth, address, foto,  citizenship,  religion,  lastEducation, yearsOfGraduation, yearsOfEntry, gender,  docCV,  docCertificate,  docTranscripts], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUcalumni", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUcalumni", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUcalumni", "Register Success", '200');
        }
    });            
};


exports.registerUcakreditasi = function(req, res) {
    //logging
    var activity = "registerUcakreditasi";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var campusName = req.body.body.campusName;
    var campusAccreditation = req.body.body.campusAccreditation;
    var address = req.body.body.address;
    var district = req.body.body.district;
    var subdistrict = req.body.body.subdistrict;
    var city = req.body.body.city;
    var province = req.body.body.province;
    var country = req.body.body.country;
    var docOpeningApprovalLetterCampus = req.body.body.docOpeningApprovalLetterCampus;
    var docNotaryDeedofEstablishment = req.body.body.docNotaryDeedofEstablishment;
    var docRatificationCampus = req.body.body.docRatificationCampus;

    connection.query('INSERT INTO registrationAkreditasi (username, registrationDate, campusName, campusAccreditation, address, district,  subdistrict,  city,  province,  country, docOpeningApprovalLetterCampus,  docNotaryDeedofEstablishment,  docRatificationCampus) values (?,Sysdate(),?,?,?,?,?,?,?,?,?,?,?)',
    [ username, campusName, campusAccreditation, address, district,  subdistrict,  city,  province, country, docOpeningApprovalLetterCampus,  docNotaryDeedofEstablishment,  docRatificationCampus], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerUcakreditasi", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "registerUcakreditasi", "Data Not Found", '201');
        } else{
            response.httpResp("Register Success","200", res)
            loging(JSON.stringify(req.body), "registerUcakreditasi", "Register Success", '200');
        }
    });            
};

  function makePassword(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

exports.insertLupaPasswordAdmin = function(req, res) {
    //logging
    var activity = "insertLupaPasswordAdmin";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var email = req.body.body.email;
    var newPasswordCreated = makePassword(10);
    newPasswordCreated = base64Encode(newPasswordCreated);

   
    connection.query('SELECT email FROM `admin` WHERE email = ?',
    [ email], 
    function (error, rows, fields){
        console.log('jum page '+rows.length);
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "insertAdmin", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Email tidak terdaftar", "201", res);
            loging(JSON.stringify(req.body), "insertLupaPasswordAdmin", "Email tidak terdaftar", '201');
        } else{
            // var upPass = updatePasswordEnduser(newPasswordCreated, email);
            // console.log('upassRes '+upPass);
            // if (upPass == '100') {
            connection.query('UPDATE admin SET password = ? where email = ?',
            [ newPasswordCreated, email], 
            function (error, rows, fields){
            if(error){                
                console.log(error.sqlMessage);                
                response.httpResp(error.sqlMessage, "500", res);
                loging(JSON.stringify(req.body), "insertLupaPasswordAdmin", error.sqlMessage, '500');
                //response.httpResp("Password anda gagal terupdate, coba beberapa saat lagi", "201", res)
            }else {
                //send email
                var nodemailer = require('nodemailer');

                var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'denyutama25@gmail.com',
                    pass: '25091994'
                }
                });

                var mailOptions = {
                from: 'denyutama25@gmail.com',
                to: email,
                subject: 'Reset Password ucc app',
                text: 'Password baru anda adalah: '+newPasswordCreated
                };

                transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error)        
                    loging(JSON.stringify(req.body), "insertLupaPasswordAdmin", error.sqlMessage, '500');
                    logger.debug(error);
                    response.httpResp(error,"202", res)
                } else {
                    console.log('Email sent: ' + info.response);
                    response.httpResp("Berhasil mengirimkan reset password!","200", res);
                    loging(JSON.stringify(req.body), "insertLupaPasswordAdmin", "Berhasil mengirimkan reset password!", '200');
                }
                });
                //end send email
            
            }
            });
        }
    });
};

function loging(req, res, message, code){
    connection.query('INSERT INTO logging (date, req, res, message, code) values (Sysdate(),?,?,?,?)',
    [ req, res, message, code],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
  }

  function auditTrail(username, activity, requestId, ip, device, deviceVersion, location){
    connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?)',
    [ username, activity, requestId, ip, device, deviceVersion, location],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
  }

  function base64Encode (data){
    let buff = new Buffer(data);
    let base64data = buff.toString('base64');
  
    console.log('"' + data + '" converted to Base64 is "' + base64data + '"');
    return base64data;
  }
  
  function base64Decode (data){
    let buff = new Buffer(data, 'base64');
    let text = buff.toString('ascii');
    
    console.log('"' + data + '" converted from Base64 to ASCII is "' + text + '"');
    return text;
  }