'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');

  exports.masterAdminAccess = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM masterAdminAccess',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.giveUserAccessToMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var menuId = req.body.body.menuId;
    var status = req.body.body.status;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updateDate = req.body.body.updateDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var username = req.body.body.username;

  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO userAccess (accessMenuId, status, createdDate, expiredDate, updateDate, createdBy, updatedBy, username) values (?,?,?,?,?,?,?,?)',
            [ menuId, status, createdDate, expiredDate, updateDate, createdBy, updatedBy, username ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User to access menu", res)
                }
            });   
        }
      });
  };
  
  exports.inquiryAdminMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from uccapp.adminMenu where groupMenu = (SELECT accessId FROM uccapp.adminRole where username = ?)',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

exports.inquiryadminSubMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from adminSubMenu where idMenu = ?',
            [ idMenu ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.addMenuEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var menuTittle = req.body.body.menuTittle;
    var menuImage = req.body.body.menuImage;
    var note = req.body.body.note;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var status = req.body.body.status;
    var priority = req.body.body.priority;
    var groupMenu = "1";//req.body.body.groupMenu;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO menu (menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu) values (?,?,?,?,?,?,?,?)',
            [ menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User menu", res)
                }
            });   
        }
      });
  };

  exports.addSubMenuEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var submenuName = req.body.body.submenuName;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
    var note = req.body.body.note;
    var createdBy = req.body.body.createdBy;
    var idmenu = req.body.body.idmenu;
    var priority = req.body.body.priority;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (submenuName, createdDate, expiredDate, updateDate, updatedBy, status, note, createdBy, idmenu, priority) values (?,?,?,?,?,?,?,?,?)',
            [ submenuName, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy, idmenu, priority ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User menu", res)
                }
            });   
        }
      });
  };


  exports.addMenuAdmin = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    //var idMenu = req.body.body.idMenu;
    var menuTittle = req.body.body.menuTittle;
    var menuImage = req.body.body.menuImage;
    var note = req.body.body.note;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var status = req.body.body.status;
    var priority = req.body.body.priority;
    var IdAdminAccess = req.body.body.IdAdminAccess;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO adminMenu ( menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu) values (?,?,?,?,?,?,?,?)',
            [ menuTittle, menuImage, note, createdDate, expiredDate, status, priority, IdAdminAccess ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add menu admin", res)
                }
            });   
        }
      });
  };

  
  exports.addSubMenuAdmin = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var submenuName = req.body.body.submenuName;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
    var note = req.body.body.note;
    var createdBy = req.body.body.createdBy;
    var priority = req.body.body.priority;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (idMenu, submenuName, createdDate, expiredDate, updateDate, updatedBy, status, note, createdBy, priority) values (?,?,?,?,?,?,?,?,?,?)',
            [ idMenu, submenuName, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy, priority ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User menu", res)
                }
            });   
        }
      });
  };

