'use strict';

var response = require('../../handling/res');
var connection = require('../../config/conn');
var logger = require('../../logger/logger');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

exports.config = function(req, res) {
    
    connection.query('SELECT * FROM `config`',
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == "" || rows == null || rows.length <= 0){
            response.httpResp("Config Information Not Found", "201", res)
        } else{       
            response.httpResp(rows, 200, res);
        }
    }); 
};

exports.initChace = function(req, res) {
    connection.query('SELECT * FROM `config`',
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == "" || rows == null || rows.length <= 0){
            response.httpResp("config Information Not Found", "201", res)
        } else{
            for (const property in rows) {
                //console.log(`${property}: ${rows[property].value}`);
                myCache.set( rows[property].key, rows[property].value);
              }       
            response.httpResp(rows, 200, res);
        }
    }); 
};