'use strict';

var response = require('../../handling/res');
var connection = require('../../config/conn');
var logger = require('../../logger/logger');

exports.auditTrail = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.header.username;
    var requestId = req.body.header.requestId;
    var requestTime = req.body.header.requestTime;
    var activity = req.body.header.activity;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;

  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?,?)',
            [ username, activity, requestId, ip, device, deviceVersion, location],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Audit Trail Information Not Found", "201", res)
                } else{
                    response.ok("Success add", res)
                }
            });   
        }
      });
  };