'use strict';

var response = require('../../handling/res');
var connection = require('../../config/conn');
var logger = require('../../logger/logger');
var jwt = require('jsonwebtoken');

exports.inquiryNewsCategory = function(req, res) {
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM uccapp.masterAdminAccess',
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("News Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

exports.inquiryNews = function(req, res) {

  var token = req.body.header.token;
  var jwtKey = req.body.header.jwtKey;
  var newsCategory = req.body.body.newsCategory;
  var page = req.body.body.page;
  var paginPage = req.body.body.paginPage;
  var rowLimit = 0;
  var rowall = 1000;
  var totalRowCount = '';
  var totalPage = '';
  var rowLimit = '';
  var rowAll = '';
    connection.query('select count(idnews) as count from uccapp.news',
    [ newsCategory ], 
    function (error2, rows2, fields2){
        if(error2){
            console.log(error2)
            logger.debug(error2)
            response.httpResp(error2.sqlMessage, "500", res)
        }if(rows2 == "" || rows2 == null || rows2.length <= 0){
            response.httpResp("News Information Not Found", "201", res)
        } else{
          let totalrowcount = rows2[0].count;
          let totalPage = Math.ceil(totalrowcount/paginPage);
          rowLimit = paginPage*page-paginPage;
          rowAll = paginPage;

          connection.query('SELECT * FROM uccapp.news where newsCategoryId = (SELECT idmasterAdminAccess FROM uccapp.masterAdminAccess where AdminAccessName = ?) and status = "active" LIMIT '+rowLimit+','+rowAll,
          [ newsCategory ], 
          function (error, rows, fields){
              if(error){
                  console.log(error)
                  logger.debug(error)
                  response.httpResp(error.sqlMessage, "500", res)
              }if(rows == "" || rows == null || rows.length <= 0){
                  response.httpResp("News Information Not Found", "201", res)
              } else{
                  response.httpRespCus(JSON.parse('{"totalRowCount" : "'+totalrowcount+'","totalPage" : "'+totalPage+'" }'), rows, "200",res)
              }
          });
        }
    });   
};

exports.addnews = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var newsTittle = req.body.body.newsTittle;
    var newsImage = req.body.body.newsImage;
    var newsCategoryId = req.body.body.newsCategoryId;
    var newsContent = req.body.body.newsContent;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO news (newsTittle, newsImage, newsCategoryId, newsContent, createdDate, expiredDate, status) values (?,?,?,?,?,?,?)',
            [ newsTittle, newsImage, newsCategoryId, newsContent, createdDate, expiredDate, status],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add", res)
                }
            });   
        }
      });
  };

  exports.updatenews = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idnews = req.body.body.idnews;
    var newsTittle = req.body.body.newsTittle;
    var newsImage = req.body.body.newsImage;
    var newsCategoryId = req.body.body.newsCategoryId;
    var newsContent = req.body.body.newsContent;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;


    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{    
            connection.query('UPDATE news SET newsTittle = ?, newsImage = ?, newsCategoryId = ?, newsContent = ?, createdDate = ?, expiredDate = ?, status = ? where idnews = ?',
            [ newsTittle, newsImage, newsCategoryId, newsContent, createdDate, expiredDate, status, idnews ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Update Success","200", res)
                }
            });
        }
    });            
};

exports.deletenews = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idnews = req.body.body.idnews;

    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{    
            connection.query('DELETE FROM uccapp.news where idnews = ?',
            [ idnews ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Delete Success","200", res)
                }
            });
        }
    });            
};
