'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');

exports.inquiryMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM admin.ucc_menu',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                    loging(JSON.stringify(req.body), "inquiryMenu", error.sqlMessage, '500');
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.enduserAccessMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT a.username, a.userAccessId, a.accessMenuId, b.menuTittle, a.status, a.createdBy, a.expiredDate FROM uccapp.userAccess a, uccapp.menu b where a.accessMenuId = b.idmenu and a.username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                    loging(JSON.stringify(req.body), "enduserAccessMenu", error.sqlMessage, '500');
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.inquirySubMenuEnduser = function(req, res) {
    //logging
    var activity = "inquirySubMenuEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var username = req.body.body.username;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from submenu where idmenu = ? and idmenu = (select distinct accessMenuId from userAccess where username = ? and accessMenuId = ?)',
            [ idMenu, username, idMenu], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                    loging(JSON.stringify(req.body), "inquirySubMenuEnduser", error.sqlMessage, '500');
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("Sorry, You not have access for this menu, please register on http://ucc-edu.com", "201", res)
                    loging(JSON.stringify(req.body), "inquirySubMenuEnduser", "Sorry, You not have access for this menu, please register on http://ucc-edu.com", '201');
                } else{
                    response.ok(rows, res)
                    loging(JSON.stringify(req.body), "inquirySubMenuEnduser", rows, '200');
                }
            });   
        }
      });
  };

  exports.addSubMenuEnduser = function(req, res) {
    //logging
    var activity = "addSubMenuEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    
    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;

    var idmenu = req.body.body.idmenu;
    var submenuName = req.body.body.submenuName;
    var note = req.body.body.note;
    var image = req.body.body.image;
    var priority = req.body.body.priority;
    var status = req.body.body.status;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (idmenu, submenuName, note, image, priority, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy) values (?,?,?,?,?,?,?,?,?,?,?)',
            [ idmenu, submenuName, note, image, priority, createdDate, expiredDate, updatedDate, updatedBy, status, createdBy],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "addSubMenuEnduser", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "addSubMenuEnduser", "User Information Not Found", '201');                    
                } else{
                    response.ok("Success add", res)
                    loging(JSON.stringify(req.body), "addSubMenuEnduser", "Success add", '200');                    
                }
            });   
        }
      });
  };

  exports.updateSubMenuEnduser = function(req, res) {
    //logging
    var activity = "updateSubMenuEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;

    var idsubmenu = req.body.body.idsubmenu;
    var idmenu = req.body.body.idmenu;
    var submenuName = req.body.body.submenuName;
    var note = req.body.body.note;
    var image = req.body.body.image;
    var priority = req.body.body.priority;
    var status = req.body.body.status;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;


    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{    
            connection.query('UPDATE submenu SET idmenu = ?, submenuName = ?, note = ?, image = ?, priority = ?, status = ?, createdDate = ?, expiredDate = ?, updatedDate = ?, updatedBy = ?, createdBy = ? where idsubmenu = ?',
            [ idmenu, submenuName, note, image, priority, status, createdDate, expiredDate, updatedDate, updatedBy, createdBy, idsubmenu ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "updateSubMenuEnduser", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Update Success","200", res)
                }
            });
        }
    });            
};

exports.deleteSubMenuEnduser = function(req, res) {
    //logging
    var activity = "deleteSubMenuEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);
    

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idsubmenu = req.body.body.idsubmenu;

    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{    
            connection.query('DELETE FROM uccapp.submenu where idsubmenu = ?',
            [ idsubmenu ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    loging(JSON.stringify(req.body), "deleteSubMenuEnduser", error, '500');
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Delete Success","200", res)
                }
            });
        }
    });            
};


function loging(req, res, message, code){
    connection.query('INSERT INTO logging (date, req, res, message, code) values (Sysdate(),?,?,?,?)',
    [ req, res, message, code],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
  }
  
function auditTrail(username, activity, requestId, ip, device, deviceVersion, location){
    connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?)',
    [ username, activity, requestId, ip, device, deviceVersion, location],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
}