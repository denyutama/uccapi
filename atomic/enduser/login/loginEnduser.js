'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');
const axios = require('axios');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

//defuilt if cache not up
var keyJwt = "campusManagementConsorsium";
var urlGoogleAuth = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=";
var urlFaceBookAuth = "https://graph.facebook.com/me?access_token=";

function AuthRequestGet(pathReq) {
  let data = '';
  axios.get(pathReq, {timeout:50000})
  .then(res => {
    data = res.data;
    //setTimeout(googleAuthxInsert, 7500, res.data.name, res.data.email);
    googleAuthxInsert(res.data.name, res.data.email);        
  })
  .catch(err => {
    console.log(err);
  });   
    return data;
 }

//sign with RSA SHA256
function generateToken(payload, expires, cb){
    // var value = myCache.get( "keyJwtCache" );
    // if ( value != undefined ){
    //     var keyJwt = value;
    // }

    jwt.sign(payload, keyJwt, {expiresIn: expires}, (err, token) => {
      if(err){
        return cb(err, null);
      }
  
      return cb(null, token);
    });
  }
  
  exports.loginEnduser = function(req, res) {
    
    //logging
    var activity = "loginEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

  
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);
  
    connection.query('SELECT * FROM `enduser` WHERE username = ? or email = ? or telephone = ?',
    [ username, username, username ],
    function (error, rows, fields){
      var usernameDB =  "null";
      var passwordDB = "null";
      var emailDB = "null";
      var telephoneDB = "null";
  
      if(rows.length >= 1){
        usernameDB = rows[0].username;
        emailDB = rows[0].email;
        telephoneDB = rows[0].telephone;
        passwordDB = rows[0].password;
      }
      
      if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }else if(username != usernameDB){
             if(username != emailDB){
                if(username != telephoneDB){
                  response.httpResp("username invalid", "202", res)           
                }else if(password != passwordDB){
                  response.httpResp("password invalid", "203", res)
                }else if(rows == "" || rows == null || rows.length <= 0){
                      response.httpResp("username or password invalid", "201", res)
                }else{
                    generateToken({username: req.body.username}, 60 * 60, (err, token) => {
                        if(err){
                            response.httpResp("Generate Token Invalid", "203", res)
                        }
                        response.ok(token, res)
                      });
                }
            }else if(password != passwordDB){
              response.httpResp("password invalid", "203", res)
            }else if(rows == "" || rows == null || rows.length <= 0){
                  response.httpResp("username or password invalid", "201", res)
            }else{
                generateToken({username: req.body.username}, 60 * 60, (err, token) => {
                    if(err){
                        response.httpResp("Generate Token Invalid", "203", res)
                    }
                    response.ok(token, res)
                  });
            }
            
            
  
        }else if(password != passwordDB){
             response.httpResp("password invalid", "203", res)
        }else if(rows == "" || rows == null || rows.length <= 0){
             response.httpResp("username or password invalid", "201", res)
        }else{
            generateToken({username: req.body.username}, 60 * 60, (err, token) => {
                if(err){
                    response.httpResp("Generate Token Invalid", "203", res)
                }
                response.ok(token, res)
              });
        }
    });
  };
  
  

  exports.loginEnduser2 = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);
     connection.query('SELECT * FROM `enduser` WHERE username = ? and password = ?',
     [ username, password ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }else if(username == rows.username ){
             response.httpResp("username invalid", "202", res)
        }else if(password == rows.password ){
             response.httpResp("password invalid", "203", res)
        }else if(rows == "" || rows == null || rows.length <= 0){
            response.httpResp("username or password invalid", "201", res)
        }else{
            generateToken({username: req.body.username}, 60 * 5, (err, token) => {
                if(err){
                    response.httpResp("Generate Token Invalid", "203", res)
                }
                response.ok(token, res)
              });
        }
    });
};

function googleAuthx(pathReq) {
    var responseData = '';
    var username = '';
    var password = 'Z3VjYw==';
    var name = '';
    var email = '';
    var status = "active";
    var telephone = '';
    
    axios.get(pathReq, {timeout:50000})
    .then(res => {
        //awal res
        var nameOri = "gucc."+res.data.name;
        name = res.data.name
        email = res.data.email
        username = nameOri.toLowerCase().replace(" ", ".");  
        console.log(username);
        //validasi user
        connection.query('SELECT * FROM `enduser` WHERE username = ? and password = ?',
        [ username, password ], 
        function (error, rows, fields){
            if(error){
                console.log(error)
                logger.debug(error)
                responseData = error.sqlMessage;
            }else if(rows == "" || rows == null || rows.length <= 0){                
                // //insert user
                connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
                [ username, password, name, email, status,  telephone ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        responseData = error.sqlMessage;
                    }
                });
                responseData = "daftar baru";
            }else{
                responseData = "userValid";
            }
            console.log("output :   "+responseData)
        });
        //selesai res
    })
    .catch(err => {
      console.log(err);
    });

    return responseData;
 }

 function googleAuthxInsert(name, email) {
    var responseData = '';
    var username = '';
    var password = 'Z3VjYw==';
    var status = "active";
    var telephone = '';
    
        //awal res
        var nameOri = "gucc."+name;
        var emailOri = ("gucc."+email).split('@');
        name = name;
        email = email;
        //username = nameOri.toLowerCase().replace(" ", ".");
        username = emailOri[0];

        //validasi user
        connection.query('SELECT * FROM `enduser` WHERE username = ? and password = ?',
        [ username, password ], 
        function (error, rows, fields){
            if(error){
                console.log(error)
                logger.debug(error)
                responseData = error.sqlMessage;
            }else if(rows == "" || rows == null || rows.length <= 0){                
                // //insert user
                connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
                [ username, password, name, email, status,  telephone ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        responseData = error.sqlMessage;
                    }
                });
                responseData = "daftar baru";
            }else{
                responseData = "userValid";
            }
            console.log("output :   "+responseData)
        });
        //selesai res
    return responseData;
 }


exports.loginEnduserByGoogle = function(req, res) {
    var value = myCache.get( "urlGoogleAuthCache" );
    if ( value != undefined ){
        var urlGoogleAuth = value;
    } 

    var token = req.body.body.tokenIdGoogle;
    var email = "gucc."+req.body.body.email;
    var username = email.split('@');
    AuthRequestGet(urlGoogleAuth+token); 
    
        generateToken({username: req.body.username}, 60 * 5, (err, token) => {
            if(err){
                response.httpResp("Generate Token Invalid", "203", res)
            }
            let dataRes = '{"username" : "'+username[0]+'", "token" : "'+token+'"}';
            response.ok(JSON.parse(dataRes), res)
          });
};

function base64Encode (data){
    let buff = new Buffer(data);
    let base64data = buff.toString('base64');
  
    console.log('"' + data + '" converted to Base64 is "' + base64data + '"');
    return base64data;
  }
  
  function base64Decode (data){
    let buff = new Buffer(data, 'base64');
    let text = buff.toString('ascii');
    
    console.log('"' + data + '" converted from Base64 to ASCII is "' + text + '"');
    return text;
  }

  function auditTrail(username, activity, requestId, ip, device, deviceVersion, location){
    connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?)',
    [ username, activity, requestId, ip, device, deviceVersion, location],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
  }
  