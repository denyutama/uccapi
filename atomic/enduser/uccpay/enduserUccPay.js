'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');

exports.inquiryUccPay = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM `uccPay` WHERE username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.inquiryTrxHistoryUccPay = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM `uccPayTrxHistory` WHERE username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };