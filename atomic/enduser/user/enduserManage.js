'use strict';

var response = require('../../../handling/res');
var connection = require('../../../config/conn');
var logger = require('../../../logger/logger');
var jwt = require('jsonwebtoken');

//future store in redis in prod
var emailForm  = 'denyutama25@gmail.com';
var emailPass = '25091994';

exports.inquiryEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;

    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM `enduser` WHERE username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                    loging(JSON.stringify(req.body), "inquiryEnduser", error.sqlMessage, '500');
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                    loging(JSON.stringify(req.body), "inquiryEnduser", "User Information Not Found", '201');
                } else{
                    response.ok(rows, res)
                    loging(JSON.stringify(req.body), "inquiryEnduser", rows, '200');
                }
            });   
        }
      });
};


exports.selfUpdateEnduser = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);

    var name = req.body.body.name;
    var email = req.body.body.email;
    var telephone = req.body.body.telephone;

    connection.query('UPDATE enduser SET password = ?, name = ?, email = ?, telephone = ? where username = ?',
    [ password, name, email, telephone,  username ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "selfUpdateEnduser", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "201", res)
            loging(JSON.stringify(req.body), "selfUpdateEnduser", "Data Not Found", '201');
        } else{
            response.httpResp("Update Success","200", res)
            loging(JSON.stringify(req.body), "selfUpdateEnduser", rows, '200');
        }
    });            
};

exports.registerEnduser = function(req, res) {
    //logging
    var activity = "registerEnduser";
    var requestId = req.body.header.requestId;
    var ip = req.body.header.ip;
    var device = req.body.header.device;
    var deviceVersion = req.body.header.deviceVersion;
    var location = req.body.header.location;
    var usernameheader = req.body.header.username;
    auditTrail(usernameheader,activity,requestId,ip,device,deviceVersion,location);

    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);
    var name = req.body.body.name;
    var email = req.body.body.email;
    var status = "active";//req.body.body.status;
    var createdDate = Date.now();//req.body.body.createdDate;
    var expiredDate = Date.now()+1;//req.body.body.expiredDate;
    var telephone = req.body.body.telephone;
    var accessId = req.body.body.accessId;

    connection.query('SELECT * FROM `enduser` WHERE username = ? or email = ? or telephone = ?',
    [ username, email, telephone ],
    function (error, rows, fields){
      var usernameDB =  "null";
      var emailDB = "null";
      var telephoneDB = "null";
  
      if(rows.length >= 1){
        usernameDB = rows[0].username;
        emailDB = rows[0].email;
        telephoneDB = rows[0].telephone;
      }
      
      if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "registerEnduser", error.sqlMessage, '500');
        }else if(username == usernameDB){
            response.httpRespWithInfo("Sorry "+name+",username already registered,please use another username", rows,"203", res)
            loging(JSON.stringify(req.body), "registerEnduser", rows, '200');
        }else if(email == emailDB){
             response.httpRespWithInfo("Sorry "+name+",email already registered,please use another email", rows,"203", res)
             loging(JSON.stringify(req.body), "registerEnduser", rows, '200');
        }else if(telephone == telephoneDB){
            response.httpRespWithInfo("Sorry "+name+",telephone already registered,please use another telephone", rows,"203", res)
            loging(JSON.stringify(req.body), "registerEnduser", rows, '200');
        }else{
            //----
            connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
            [ username, password, name, email, status,  telephone ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                    loging(JSON.stringify(req.body), "registerEnduser", error.sqlMessage, '500');
                }if(rows == null || rows.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    if (email.length >= 1 || email != null || email != undefined || email != ""){
                        //send email
                        var nodemailer = require('nodemailer');
        
                        var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: emailForm,
                            pass: emailPass
                        }
                        });
        
                        var mailOptions = {
                        from: emailForm,
                        to: email,
                        subject: 'Welcome to ucc app',
                        text: 'Welcome to Universal Campus Consortium, your username: '+username+', your email: '+email+', your telephone: '+telephone+', your password: '+base64Decode(password)+'. Please keep secret your account information.' 
                        };
        
                        transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                            console.log(error)
                            logger.debug(error);
                            response.httpResp(error,"202", res)
                            loging(JSON.stringify(req.body), "registerEnduser", error.response, '202');
                        } else {
                            console.log('Email sent: ' + info.response);
                            //response.httpResp("Berhasil mengirimkan reset password!","200", res)
                            loging(JSON.stringify(req.body), "registerEnduser", info.response, '200');
                        }
                        });
                        //end send email                    
                    }
                    response.httpResp("Register Success","200", res)
                    loging(JSON.stringify(req.body), "registerEnduser", "Register Success", '200');
                }
            });    
            //---
        }
    });
  };

exports.registerEnduser2 = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    password = base64Encode(password);

    var name = req.body.body.name;
    var email = req.body.body.email;
    var status = "active";//req.body.body.status;
    var createdDate = Date.now();//req.body.body.createdDate;
    var expiredDate = Date.now()+1;//req.body.body.expiredDate;
    var telephone = req.body.body.telephone;

    connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
    [ username, password, name, email, status,  telephone ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "enduserAccessMenu", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "200", res)
        } else{
            response.httpResp("Register Success","200", res)
        }
    });            
};

  function makePassword(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

  exports.insertLupaPasswordEnduser = function(req, res) {
    
    var email = req.body.body.email;
    var newPasswordCreated = makePassword(10);
    newPasswordCreated = base64Encode(newPasswordCreated);

    connection.query('SELECT email FROM `enduser` WHERE email = ?',
    [ email], 
    function (error, rows, fields){
        console.log('jum page '+rows.length);
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
            loging(JSON.stringify(req.body), "enduserAccessMenu", error.sqlMessage, '500');
        }if(rows == null || rows.length <= 0){
            response.httpResp("Email tidak terdaftar", "201", res)
        } else{
            connection.query('UPDATE enduser SET password = ? where email = ?',
            [ newPasswordCreated, email], 
            function (error, rows, fields){
            if(error){
                console.log(error.sqlMessage);                
                response.httpResp(error.sqlMessage, "500", res)
                    loging(JSON.stringify(req.body), "enduserAccessMenu", error.sqlMessage, '500');
                //response.httpResp("Password anda gagal terupdate, coba beberapa saat lagi", "201", res)
            }else {
                //send email
                var nodemailer = require('nodemailer');

                var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: emailForm,
                    pass: emailPass
                }
                });

                var mailOptions = {
                from: emailForm,
                to: email,
                subject: 'Reset Password ucc app',
                text: 'Your New Password is: '+newPasswordCreated
                };

                transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error)
                logger.debug(error);
                    response.httpResp(error,"202", res)
                } else {
                    console.log('Email sent: ' + info.response);
                    response.httpResp("Berhasil mengirimkan reset password!","200", res)
                }
                });
                //end send email
            
            }
            });
        }
    });
};

function loging(req, res, message, code){
    connection.query('INSERT INTO logging (date, req, res, message, code) values (Sysdate(),?,?,?,?)',
    [ req, res, message, code],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
  }
  
function auditTrail(username, activity, requestId, ip, device, deviceVersion, location){
    connection.query('INSERT INTO auditTrail (activityDate, username, activity, requestId, ip, device, deviceVersion, location) values (Sysdate(),?,?,?,?,?,?,?)',
    [ username, activity, requestId, ip, device, deviceVersion, location],          
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
        }
    });
}

function base64Encode (data){
    let buff = new Buffer(data);
    let base64data = buff.toString('base64');
  
    console.log('"' + data + '" converted to Base64 is "' + base64data + '"');
    return base64data;
  }
  
  function base64Decode (data){
    let buff = new Buffer(data, 'base64');
    let text = buff.toString('ascii');
    
    console.log('"' + data + '" converted from Base64 to ASCII is "' + text + '"');
    return text;
  }
