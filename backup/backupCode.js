'use strict';

var response = require('../handling/res');
var connection = require('../config/conn');
var chaceInit = require('../config/cache');
var logger = require('../logger/logger');
const model = require('../models/index');
var jwt = require('jsonwebtoken');
const https = require('https')
const axios = require('axios');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

//defuilt if cache not up
var keyJwt = "Y2FtcHVzTWFuYWdlbWVudENvbnNvcnNpdW0=";
var urlGoogleAuth = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=";
var urlFaceBookAuth = "https://graph.facebook.com/me?access_token=";

exports.index = function(req, res) {
    response.httpResp("welocome to ucc api", "200", res);
};

exports.config = function(req, res) {
    
    connection.query('SELECT * FROM `config`',
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == "" || rows == null || rows.length <= 0){
            response.httpResp("User Information Not Found", "201", res)
        } else{       
            response.httpResp(rows, 200, res);
        }
    }); 
};

exports.initChace = function(req, res) {
    connection.query('SELECT * FROM `config`',
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == "" || rows == null || rows.length <= 0){
            response.httpResp("User Information Not Found", "201", res)
        } else{
            for (const property in rows) {
                //console.log(`${property}: ${rows[property].value}`);
                myCache.set( rows[property].key, rows[property].value);
              }       
            response.httpResp(rows, 200, res);
        }
    }); 
};

exports.getKey = function(req, res) {
    var value = myCache.get( "urlGoogleAuthCache" );
    if ( value != undefined ){
        var urlGoogleAuth = value;
    }
    response.httpResp(keyJwt, "200", res);
};


function httpsRequestGet(pathReq) {
    let data = '';
    console.log("url: "+pathReq);
  axios.get(pathReq, {timeout:50000})
  .then(res => {
    data = res.data;
    //setTimeout(googleAuthxInsert, 7500, res.data.name, res.data.email);
    googleAuthxInsert(res.data.name, res.data.email);        
  })
  .catch(err => {
    console.log(err);
  });   
    return data;
 }

 function httpsRequestPost(length) {
    const https = require('https')

    const data = JSON.stringify({
        todo: 'ucc post req'
    })

    const options = {
        hostname: 'whatever.com',
        port: 443,
        path: '/todos',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        }
    }

    const req = https.request(options, res => {
        console.log(`statusCode: ${res.statusCode}`)

        res.on('data', d => {
            process.stdout.write(d)
        })
    })

    req.on('error', error => {
        console.error(error)
    })

    req.write(data)
    req.end()

    return result;
 }


//sign with RSA SHA256
function generateToken(payload, expires, cb){
    jwt.sign(payload, keyJwt, {expiresIn: expires}, (err, token) => {
      if(err){
        return cb(err, null);
      }
  
      return cb(null, token);
    });
  }
  
exports.loginEnduser = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    
     connection.query('SELECT * FROM `enduser` WHERE username = ? and password = ?',
     [ username, password ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }else if(rows == "" || rows == null || rows.length <= 0){
            response.httpResp("username or password invalid", "201", res)
        }else if(username == rows.username ){
             response.httpResp("username invalid", "202", res)
        }else if(password == rows.password ){
             response.httpResp("password invalid", "203", res)
        }else{
            generateToken({username: req.body.username}, 60 * 5, (err, token) => {
                if(err){
                    response.httpResp("Generate Token Invalid", "203", res)
                }
                response.ok(token, res)
              });
        }
    });
};

function googleAuthx(pathReq) {
    var responseData = '';
    var username = '';
    var password = 'Z3VjYw==';
    var name = '';
    var email = '';
    var status = "active";
    var telephone = '';
    
    axios.get(pathReq, {timeout:50000})
    .then(res => {
        //awal res
        var nameOri = "gucc."+res.data.name;
        name = res.data.name
        email = res.data.email
        username = nameOri.toLowerCase().replace(" ", ".");  
        console.log(username);
        //validasi user
        connection.query('SELECT * FROM `enduser` WHERE username = ? and password = ?',
        [ username, password ], 
        function (error, rows, fields){
            if(error){
                console.log(error)
                logger.debug(error)
                responseData = error.sqlMessage;
            }else if(rows == "" || rows == null || rows.length <= 0){                
                // //insert user
                connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
                [ username, password, name, email, status,  telephone ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        responseData = error.sqlMessage;
                    }
                });
                responseData = "daftar baru";
            }else{
                responseData = "userValid";
            }
            console.log("output :   "+responseData)
        });
        //selesai res
    })
    .catch(err => {
      console.log(err);
    });

    return responseData;
 }

 function googleAuthxInsert(name, email) {
    var responseData = '';
    var username = '';
    var password = 'Z3VjYw==';
    var status = "active";
    var telephone = '';
    
        //awal res
        var nameOri = "gucc."+name;
        var emailOri = ("gucc."+email).split('@');
        name = name;
        email = email;
        //username = nameOri.toLowerCase().replace(" ", ".");
        username = emailOri[0];

        //validasi user
        connection.query('SELECT * FROM `enduser` WHERE username = ? and password = ?',
        [ username, password ], 
        function (error, rows, fields){
            if(error){
                console.log(error)
                logger.debug(error)
                responseData = error.sqlMessage;
            }else if(rows == "" || rows == null || rows.length <= 0){                
                // //insert user
                connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
                [ username, password, name, email, status,  telephone ], 
                function (error, rows, fields){
                    if(error){
                        console.log(error)
                        logger.debug(error)
                        responseData = error.sqlMessage;
                    }
                });
                responseData = "daftar baru";
            }else{
                responseData = "userValid";
            }
            console.log("output :   "+responseData)
        });
        //selesai res
    return responseData;
 }


exports.loginEnduserByGoogle = function(req, res) {
    var value = myCache.get( "urlGoogleAuthCache" );
    if ( value != undefined ){
        var urlGoogleAuth = value;
    } 

    var token = req.body.body.tokenIdGoogle;
    var email = "gucc."+req.body.body.email;
    var username = email.split('@');
    httpsRequestGet(urlGoogleAuth+token); 
    
        generateToken({username: req.body.username}, 60 * 5, (err, token) => {
            if(err){
                response.httpResp("Generate Token Invalid", "203", res)
            }
            let dataRes = '{"username" : "'+username[0]+'", "token" : "'+token+'"}';
            response.ok(JSON.parse(dataRes), res)
          });
};

exports.loginAdmin = function(req, res) {
    
  var username = req.body.body.username;
  var password = req.body.body.password;
   connection.query('SELECT * FROM `admin` WHERE username = ? and password = ?',
   [ username, password ], 
  function (error, rows, fields){
      if(error){
          console.log(error)
          logger.debug(error)
          response.httpResp(error.sqlMessage, "500", res)
      }else if(rows == "" || rows == null || rows.length <= 0){
          response.httpResp("username or password invalid", "201", res)
      }else if(username == rows.username ){
           response.httpResp("username invalid", "202", res)
      }else if(password == rows.password ){
           response.httpResp("password invalid", "203", res)
      }else{
          generateToken({username: req.body.username}, 60 * 5, (err, token) => {
              if(err){
                  response.httpResp("Generate Token Invalid", "203", res)
              }
              response.ok(token, res)
            });
      }
  });
};

exports.inquiryEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;

    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM `enduser` WHERE username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
};

exports.inquiryAdmin = function(req, res) {

  var token = req.body.header.token;
  var jwtKey = req.body.header.jwtKey;
  var username = req.body.body.username;

  jwt.verify(token, jwtKey, function(err, decoded) {
      console.log(decoded) // content in jwt
      if(err){
          response.httpResp(err, "599", res)
      }else{
          connection.query('SELECT * FROM `admin` WHERE username = ?',
          [ username ], 
          function (error, rows, fields){
              if(error){
                  console.log(error)
                  logger.debug(error)
                  response.httpResp(error.sqlMessage, "500", res)
              }if(rows == "" || rows == null || rows.length <= 0){
                  response.httpResp("User Information Not Found", "201", res)
              } else{
                  response.ok(rows, res)
              }
          });   
      }
    });
};

exports.selfUpdateEnduser = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    var name = req.body.body.name;
    var email = req.body.body.email;
    var telephone = req.body.body.telephone;

    connection.query('UPDATE enduser SET password = ?, name = ?, email = ?, telephone = ? where username = ?',
    [ password, name, email, telephone,  username ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "200", res)
        } else{
            response.httpResp("Update Success","200", res)
        }
    });            
};

exports.selfUpdateAdmin = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    var name = req.body.body.name;
    var email = req.body.body.email;
    var telephone = req.body.body.telephone;

    connection.query('UPDATE admin SET password = ?, name = ?, email = ?, telephone = ? where username = ?',
    [ password, name, email, telephone,  username ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "200", res)
        } else{
            response.httpResp("Update Success","200", res)
        }
    });            
};


exports.registerEnduser = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    var name = req.body.body.name;
    var email = req.body.body.email;
    var status = "active";//req.body.body.status;
    var createdDate = Date.now();//req.body.body.createdDate;
    var expiredDate = Date.now()+1;//req.body.body.expiredDate;
    var telephone = req.body.body.telephone;

    connection.query('INSERT INTO enduser (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
    [ username, password, name, email, status,  telephone ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "200", res)
        } else{
            response.httpResp("Register Success","200", res)
        }
    });            
};

exports.registerAdmin = function(req, res) {
    
    var username = req.body.body.username;
    var password = req.body.body.password;
    var name = req.body.body.name;
    var email = req.body.body.email;
    var status = "active";//req.body.body.status;
    var createdDate = Date.now();//req.body.body.createdDate;
    var expiredDate = Date.now()+1;//req.body.body.expiredDate;
    var telephone = req.body.body.telephone;
    var accessId = req.body.body.accessId;

    connection.query('INSERT INTO admin (username, password, name, email, status, createdDate, expiredDate, telephone) values (?,?,?,?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
    [ username, password, name, email, status,  telephone ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Data Not Found", "200", res)
        } else{
            connection.query('INSERT INTO adminRole (accessId, status, createdDate, expiredDate, username) values (?,?,Sysdate(),Sysdate()+ INTERVAL 10 YEAR,?)',
            [ accessId, status, username ],
            function (error2, rows2, fields2){
                if(error2){
                    console.log(error2)
                    logger.debug(error2)
                    response.httpResp(error2.sqlMessage, "500", res)
                }if(rows2 == null || rows2.length <= 0){
                    response.httpResp("Data Not Found", "200", res)
                } else{
                    response.httpResp("Register Success","200", res)
                }
            });
        }
    });            
};

exports.inquiryUccPay = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM `uccPay` WHERE username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.inquiryTrxHistoryUccPay = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM `uccPayTrxHistory` WHERE username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };


  exports.inquiryMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM menu',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.masterAdminAccess = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM masterAdminAccess',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.enduserAccessMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT a.username, a.userAccessId, a.accessMenuId, b.menuTittle, a.status, a.createdBy, a.expiredDate FROM uccapp.userAccess a, uccapp.menu b where a.accessMenuId = b.idmenu and a.username = ?',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.giveUserAccessToMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var menuId = req.body.body.menuId;
    var status = req.body.body.status;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updateDate = req.body.body.updateDate;
    var createdBy = req.body.body.createdBy;
    var updatedBy = req.body.body.updatedBy;
    var username = req.body.body.username;

  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO userAccess (accessMenuId, status, createdDate, expiredDate, updateDate, createdBy, updatedBy, username) values (?,?,?,?,?,?,?,?)',
            [ menuId, status, createdDate, expiredDate, updateDate, createdBy, updatedBy, username ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User to access menu", res)
                }
            });   
        }
      });
  };
  
  exports.inquiryAdminMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from uccapp.adminMenu where groupMenu = (SELECT accessId FROM uccapp.adminRole where username = ?)',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.addMenuAdmin = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    //var idMenu = req.body.body.idMenu;
    var menuTittle = req.body.body.menuTittle;
    var menuImage = req.body.body.menuImage;
    var note = req.body.body.note;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var status = req.body.body.status;
    var priority = req.body.body.priority;
    var IdAdminAccess = req.body.body.IdAdminAccess;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO adminMenu ( menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu) values (?,?,?,?,?,?,?,?)',
            [ menuTittle, menuImage, note, createdDate, expiredDate, status, priority, IdAdminAccess ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add menu admin", res)
                }
            });   
        }
      });
  };


  exports.addMenuEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var menuTittle = req.body.body.menuTittle;
    var menuImage = req.body.body.menuImage;
    var note = req.body.body.note;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var status = req.body.body.status;
    var priority = req.body.body.priority;
    var groupMenu = "1";//req.body.body.groupMenu;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO menu (menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu) values (?,?,?,?,?,?,?,?)',
            [ menuTittle, menuImage, note, createdDate, expiredDate, status, priority, groupMenu ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User menu", res)
                }
            });   
        }
      });
  };

  exports.inquiryLanguage = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var username = req.body.body.username;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('SELECT * FROM language',
            [ username ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  function makePassword(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

 function updatePasswordEnduser (pass, email) {
    console.log(pass+ ' ||| ' +email)
    connection.query('UPDATE end_user SET password = ? where email = ?',
    [ pass, email], 
    function (error, rows, fields){
        console.log(rows);
        var result = 'gagal teroos';
        if(error){
            console.log(error)
            logger.debug(error)
            result = '199'
            console.log(result);
            return result;
        }if(rows == null || rows.length <= 0){
            result = '199'
            console.log(result);
            return result;
        } else{
            console.log(rows);
            result = '100'
            console.log(result);
            return result;
        }
    });
}

  exports.insertLupaPasswordEnduser = function(req, res) {
    
    var email = req.body.body.email;
    var newPasswordCreated = makePassword(10);
   
    connection.query('SELECT email FROM `enduser` WHERE email = ?',
    [ email], 
    function (error, rows, fields){
        console.log('jum page '+rows.length);
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Email tidak terdaftar", "201", res)
        } else{
            // var upPass = updatePasswordEnduser(newPasswordCreated, email);
            // console.log('upassRes '+upPass);
            // if (upPass == '100') {
            connection.query('UPDATE enduser SET password = ? where email = ?',
            [ newPasswordCreated, email], 
            function (error, rows, fields){
            if(error){
                console.log(error.sqlMessage);                
                response.httpResp(error.sqlMessage, "500", res)
                //response.httpResp("Password anda gagal terupdate, coba beberapa saat lagi", "201", res)
            }else {
                //send email
                var nodemailer = require('nodemailer');

                var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'denyutama25@gmail.com',
                    pass: '25091994'
                }
                });

                var mailOptions = {
                from: 'denyutama25@gmail.com',
                to: email,
                subject: 'Reset Password ucc app',
                text: 'Password baru anda adalah: '+newPasswordCreated
                };

                transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error)
                logger.debug(error);
                    response.httpResp(error,"202", res)
                } else {
                    console.log('Email sent: ' + info.response);
                    response.httpResp("Berhasil mengirimkan reset password!","200", res)
                }
                });
                //end send email
            
            }
            });
        }
    });
    //console.log("insertLupaPassword req",req);
    //console.log("insertLupaPassword res",res);
};

exports.insertLupaPasswordAdmin = function(req, res) {
    
    var email = req.body.body.email;
    var newPasswordCreated = makePassword(10);
   
    connection.query('SELECT email FROM `admin` WHERE email = ?',
    [ email], 
    function (error, rows, fields){
        console.log('jum page '+rows.length);
        if(error){
            console.log(error)
            logger.debug(error)
            response.httpResp(error.sqlMessage, "500", res)
        }if(rows == null || rows.length <= 0){
            response.httpResp("Email tidak terdaftar", "201", res)
        } else{
            // var upPass = updatePasswordEnduser(newPasswordCreated, email);
            // console.log('upassRes '+upPass);
            // if (upPass == '100') {
            connection.query('UPDATE admin SET password = ? where email = ?',
            [ newPasswordCreated, email], 
            function (error, rows, fields){
            if(error){                
                console.log(error.sqlMessage);                
                response.httpResp(error.sqlMessage, "500", res)
                //response.httpResp("Password anda gagal terupdate, coba beberapa saat lagi", "201", res)
            }else {
                //send email
                var nodemailer = require('nodemailer');

                var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'denyutama25@gmail.com',
                    pass: '25091994'
                }
                });

                var mailOptions = {
                from: 'denyutama25@gmail.com',
                to: email,
                subject: 'Reset Password ucc app',
                text: 'Password baru anda adalah: '+newPasswordCreated
                };

                transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error)
                logger.debug(error);
                    response.httpResp(error,"202", res)
                } else {
                    console.log('Email sent: ' + info.response);
                    response.httpResp("Berhasil mengirimkan reset password!","200", res)
                }
                });
                //end send email
            
            }
            });
        }
    });
    //console.log("insertLupaPassword req",req);
    //console.log("insertLupaPassword res",res);
};

exports.inquiryadminSubMenu = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from adminSubMenu where idMenu = ?',
            [ idMenu ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.inquirySubMenuEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{

            connection.query('select * from submenu where idmenu = ?',
            [ idMenu ], 
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok(rows, res)
                }
            });   
        }
      });
  };

  exports.addSubMenuEnduser = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var submenuName = req.body.body.submenuName;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
    var note = req.body.body.note;
    var createdBy = req.body.body.createdBy;
    var idmenu = req.body.body.idmenu;
    var priority = req.body.body.priority;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (submenuName, createdDate, expiredDate, updateDate, updatedBy, status, note, createdBy, idmenu, priority) values (?,?,?,?,?,?,?,?,?)',
            [ submenuName, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy, idmenu, priority ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User menu", res)
                }
            });   
        }
      });
  };


  
  exports.addSubMenuAdmin = function(req, res) {

    var token = req.body.header.token;
    var jwtKey = req.body.header.jwtKey;
    var idMenu = req.body.body.idMenu;
    var submenuName = req.body.body.submenuName;
    var createdDate = req.body.body.createdDate;
    var expiredDate = req.body.body.expiredDate;
    var updatedDate = req.body.body.updatedDate;
    var updatedBy = req.body.body.updatedBy;
    var status = req.body.body.status;
    var note = req.body.body.note;
    var createdBy = req.body.body.createdBy;
    var priority = req.body.body.priority;
  
    jwt.verify(token, jwtKey, function(err, decoded) {
        console.log(decoded) // content in jwt
        if(err){
            response.httpResp(err, "599", res)
        }else{
            connection.query('INSERT INTO submenu (idMenu, submenuName, createdDate, expiredDate, updateDate, updatedBy, status, note, createdBy, priority) values (?,?,?,?,?,?,?,?,?,?)',
            [ idMenu, submenuName, createdDate, expiredDate, updatedDate, updatedBy, status, note, createdBy, priority ],          
            function (error, rows, fields){
                if(error){
                    console.log(error)
                    logger.debug(error)
                    response.httpResp(error.sqlMessage, "500", res)
                }if(rows == "" || rows == null || rows.length <= 0){
                    response.httpResp("User Information Not Found", "201", res)
                } else{
                    response.ok("Success add User menu", res)
                }
            });   
        }
      });
  };

