'use strict';

module.exports = function(app) {
    //var todoList = require('../backup/backupCode');
    var general = require('../../general/backupCode');
    var todoList = require('../../backup/backupCode');
    var todoList = require('../../backup/backupCode');
    var todoList = require('../../backup/backupCode');
    var todoList = require('../../backup/backupCode');
    var todoList = require('../../backup/backupCode');
    var todoList = require('../../backup/backupCode');
    
    
    app.route('/initChace')
    .get(todoList.initChace);
    
    app.route('/config')
    .get(todoList.config);
    
    app.route('/loginEnduserByGoogle')
    .post(todoList.loginEnduserByGoogle);
    
    app.route('/addSubMenuAdmin')
    .post(todoList.addSubMenuAdmin);    

    app.route('/addSubMenuEnduser')
    .post(todoList.addSubMenuEnduser);    

    app.route('/inquirySubMenuEnduser')
    .post(todoList.inquirySubMenuEnduser);    

    app.route('/inquiryadminSubMenu')
    .post(todoList.inquiryadminSubMenu);    

    app.route('/forgotPasswordAdmin')
    .post(todoList.insertLupaPasswordAdmin);    
    
    app.route('/forgotPasswordEnduser')
    .post(todoList.insertLupaPasswordEnduser);    
    
    app.route('/addMenuEnduser')
    .post(todoList.addMenuEnduser);
    
    app.route('/inquiryAdminMenu')
    .post(todoList.inquiryAdminMenu);

    app.route('/addMenuAdmin')
    .post(todoList.addMenuAdmin);

    app.route('/inquiryLanguage')
    .post(todoList.inquiryLanguage);

    app.route('/masterAdminAccess')
    .post(todoList.masterAdminAccess);

    app.route('/giveUserAccessToMenu')
    .post(todoList.giveUserAccessToMenu);

    app.route('/enduserAccessMenu')
    .post(todoList.enduserAccessMenu);

    app.route('/inquiryMenu')
    .post(todoList.inquiryMenu);

    app.route('/inquiryTrxHistoryUccPay')
    .post(todoList.inquiryTrxHistoryUccPay);

    app.route('/inquiryUccPay')
    .post(todoList.inquiryUccPay);

    app.route('/selfUpdateEnduser')
    .post(todoList.selfUpdateEnduser);

    app.route('/selfUpdateAdmin')
    .post(todoList.selfUpdateAdmin);

    app.route('/registerAdmin')
    .post(todoList.registerAdmin);

    app.route('/registerEnduser')
    .post(todoList.registerEnduser);

    app.route('/registerEnduser')
    .post(todoList.registerEnduser);

    app.route('/loginEnduser')
    .post(todoList.loginEnduser);

    app.route('/inquiryEnduser')
    .post(todoList.inquiryEnduser);

    app.route('/loginAdmin')
    .post(todoList.loginAdmin);

    app.route('/inquiryAdmin')
    .post(todoList.inquiryAdmin);

    app.route('/getKey')
    .get(todoList.getKey);

    app.route('/')
    .get(todoList.index);
};
