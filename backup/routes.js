'use strict';

module.exports = function(app) {
    var general = require('../../atomic/general/general');
    var cache = require('../../atomic/general/cache');

    var loginAdmin = require('../../atomic/admin/login/loginAdmin');
    var menuAdmin = require('../../atomic/admin/menu/menuAdmin');
    var uccpayAdmin = require('../../atomic/admin/uccpay/uccpayAdmin');
    var userAdmin = require('../../atomic/admin/user/manageUserForAdmin');

    var loginEnduser = require('../../atomic/enduser/login/loginEnduser');
    var menuEnduser = require('../../atomic/enduser/menu/menu');
    var uccpayEnduser = require('../../atomic/enduser/uccpay/enduserUccPay');
    var userEnduser = require('../../atomic/enduser/user/enduserManage');    
    
    //route for general     
    app.route('/')
    .get(general.index);

    app.route('/initChace')
    .get(cache.initChace);
    
    app.route('/config')
    .get(cache.config);

    app.route('/getKey')
    .get(general.getKey);

    app.route('/inquiryLanguage')
    .post(general.inquiryLanguage);
    

    //route for admin 
    //loginAdmin   
    app.route('/loginAdmin')
    .post(loginAdmin.loginAdmin);

    app.route('/registerAdmin')
    .post(userAdmin.registerAdmin);


    //userAdmin
    app.route('/inquiryAdmin')
    .post(userAdmin.inquiryAdmin);

    app.route('/selfUpdateAdmin')
    .post(userAdmin.selfUpdateAdmin);

    app.route('/forgotPasswordAdmin')
    .post(userAdmin.insertLupaPasswordAdmin);
    
    //menuAdmin
    app.route('/masterAdminAccess')
    .post(menuAdmin.masterAdminAccess);

    app.route('/inquiryAdminMenu')
    .post(menuAdmin.inquiryAdminMenu);

    app.route('/addMenuAdmin')
    .post(menuAdmin.addMenuAdmin);

    app.route('/inquiryadminSubMenu')
    .post(menuAdmin.inquiryadminSubMenu);    

    app.route('/addSubMenuAdmin')
    .post(menuAdmin.addSubMenuAdmin);    

    app.route('/giveUserAccessToMenu')
    .post(menuAdmin.giveUserAccessToMenu);

    //admin manage enduser
    app.route('/addMenuEnduser')
    .post(menuAdmin.addMenuEnduser);
    
    app.route('/addSubMenuEnduser')
    .post(menuAdmin.addSubMenuEnduser);    

    
    //route for enduser
    //login
    app.route('/loginEnduser')
    .post(loginEnduser.loginEnduser);

    app.route('/loginEnduserByGoogle')
    .post(loginEnduser.loginEnduserByGoogle);

    //userEnduser
    app.route('/registerEnduser')
    .post(userEnduser.registerEnduser);

    app.route('/inquiryEnduser')
    .post(userEnduser.inquiryEnduser);

    app.route('/selfUpdateEnduser')
    .post(userEnduser.selfUpdateEnduser);
    
    app.route('/forgotPasswordEnduser')
    .post(userEnduser.insertLupaPasswordEnduser);    
    
    //menu
    app.route('/inquiryMenu')
    .post(menuEnduser.inquiryMenu);

    app.route('/inquirySubMenuEnduser')
    .post(menuEnduser.inquirySubMenuEnduser);    
    
    app.route('/enduserAccessMenu')
    .post(menuEnduser.enduserAccessMenu);


    //uccpay
    app.route('/inquiryTrxHistoryUccPay')
    .post(uccpayEnduser.inquiryTrxHistoryUccPay);

    app.route('/inquiryUccPay')
    .post(uccpayEnduser.inquiryUccPay);

};
