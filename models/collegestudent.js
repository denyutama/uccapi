'use strict';
module.exports = (sequelize, DataTypes) => {
  const collegeStudent = sequelize.define('collegeStudent', {
    nim: {
      allowNull: false,
      //autoIncrement: true,
      primaryKey: true,
      type: DataTypes.STRING
    },
    password: DataTypes.STRING,
    name: DataTypes.STRING,
    gender: DataTypes.STRING,
    placeOfBirth: DataTypes.STRING,
    dayOfBirth: DataTypes.DATE,
    telephone: DataTypes.STRING,
    email: DataTypes.STRING,
    religion: DataTypes.STRING,
    address: DataTypes.STRING,
    majors: DataTypes.STRING,
    faculty: DataTypes.STRING,
    yearOfEntry: DataTypes.DATE
  }, {
    freezeTableName: true
  });
  collegeStudent.associate = function(models) {
    // associations can be defined here
  };
  return collegeStudent;
};