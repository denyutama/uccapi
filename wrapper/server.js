var express = require('express'),
    app = express(),
    port = 3001,
    bodyParser = require('body-parser');

    var cors = require('cors');
    app.use(cors());    

app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
    
var routes = require('../routing/routes');
routes(app);

app.listen(port);
console.log('ucc listen on : ' + port);
