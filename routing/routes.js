'use strict';

module.exports = function(app) {
    var general = require('../atomic/general/general');
    var cache = require('../atomic/general/cache');
    var news = require('../atomic/general/news');

    var loginAdmin = require('../atomic/admin/login/loginAdmin');
    var menuAdmin = require('../atomic/admin/menu/menuAdmin');
    var uccpayAdmin = require('../atomic/admin/uccpay/uccpayAdmin');
    var userAdmin = require('../atomic/admin/user/manageUserForAdmin');

    var loginEnduser = require('../atomic/enduser/login/loginEnduser');
    var menuEnduser = require('../atomic/enduser/menu/menu');
    var uccpayEnduser = require('../atomic/enduser/uccpay/enduserUccPay');
    var userEnduser = require('../atomic/enduser/user/enduserManage');    
    
    //================== route for general ==================     
    //common
    app.route('/')
    .get(general.index);
    
    app.route('/getKey')
    .get(general.getKey);

    app.route('/inquiryLanguage')
    .post(general.inquiryLanguage);

    //news
    app.route('/inquiryNews')
    .post(news.inquiryNews);

    app.route('/addNews')
    .post(news.addnews);

    app.route('/updateNews')
    .post(news.updatenews);

    app.route('/deleteNews')
    .post(news.deletenews);

    //cache
    app.route('/initChace')
    .get(cache.initChace);
    
    app.route('/config')
    .get(cache.config);
    //================== end general ==================    

    //================== route for admin ==================
    //loginAdmin   
    app.route('/loginAdmin')
    .post(loginAdmin.loginAdmin);

    app.route('/registerAdmin')
    .post(userAdmin.registerAdmin);


    //userAdmin
    app.route('/inquiryAdmin')
    .post(userAdmin.inquiryAdmin);

    app.route('/selfUpdateAdmin')
    .post(userAdmin.selfUpdateAdmin);

    app.route('/forgotPasswordAdmin')
    .post(userAdmin.insertLupaPasswordAdmin);

    //register in web admin
    app.route('/registerUccamp')
    .post(userAdmin.registerUccamp); 
    
    app.route('/registerUcteach')
    .post(userAdmin.registerUcteach); 

    app.route('/registerUcbiz')
    .post(userAdmin.registerUcbiz); 

    app.route('/registerUclearn')
    .post(userAdmin.registerUclearn); 

    app.route('/registerUcalumni')
    .post(userAdmin.registerUcalumni); 

    app.route('/registerUcakreditasi')
    .post(userAdmin.registerUcakreditasi); 

    app.route('/registerUcshop')
    .post(userAdmin.registerUcshop); 

    app.route('/registerUcjob')
    .post(userAdmin.registerUcjob); 

    //menuAdmin
    app.route('/masterAdminAccess')
    .post(menuAdmin.masterAdminAccess);

    app.route('/inquiryAdminMenu')
    .post(menuAdmin.inquiryAdminMenu);

    app.route('/addMenuAdmin')
    .post(menuAdmin.addMenuAdmin);

    app.route('/inquiryadminSubMenu')
    .post(menuAdmin.inquiryadminSubMenu);    

    app.route('/addSubMenuAdmin')
    .post(menuAdmin.addSubMenuAdmin);    

    app.route('/giveUserAccessToMenu')
    .post(menuAdmin.giveUserAccessToMenu);

    app.route('/inquiryChildSubMenu')
    .post(menuAdmin.inquiryChildSubMenu);

    app.route('/addChildSubMenu')
    .post(menuAdmin.addChildSubMenu);

    app.route('/updateChildSubMenu')
    .post(menuAdmin.updateChildSubMenu);

    app.route('/deleteChildSubMenu')
    .post(menuAdmin.deleteChildSubMenu);

    app.route('/inquiryProvince')
    .post(menuAdmin.inquiryProvince);

    app.route('/inquiryRegencies')
    .post(menuAdmin.inquiryRegencies);

    app.route('/inquiryDistricts')
    .post(menuAdmin.inquiryDistricts);

    app.route('/inquiryVillages')
    .post(menuAdmin.inquiryVillages);

    //admin manage enduser
    app.route('/addMenuEnduser')
    .post(menuAdmin.addMenuEnduser);
    
    app.route('/inquiryAccessMatrix')
    .post(menuAdmin.inquiryAccessMatrix);
    
    app.route('/addAccessMatrix')
    .post(menuAdmin.addAccessMatrix);
    
    app.route('/updateAccessMatrix')
    .post(menuAdmin.updateAccessMatrix);

    app.route('/deleteAccessMatrix')
    .post(menuAdmin.deleteAccessMatrix);

    //app.route('/addSubMenuEnduser')
    //.post(menuAdmin.addSubMenuEnduser);

    //landing page
    app.route('/inquirylandingPage')
    .post(menuAdmin.inquirylandingPage);

    app.route('/addLandingPage')
    .post(menuAdmin.addLandingPage);

    app.route('/updateLandingPage')
    .post(menuAdmin.updateLandingPage);

    app.route('/deleteLandingPage')
    .post(menuAdmin.deleteLandingPage);
    
    app.route('/inquirylandingPageMenu')
    .post(menuAdmin.inquirylandingPageMenu);
    
    app.route('/addLandingPageMenu')
    .post(menuAdmin.addLandingPageMenu);

    app.route('/updateLandingPageMenu')
    .post(menuAdmin.updateLandingPageMenu);

    app.route('/deleteLandingPageMenu')
    .post(menuAdmin.deleteLandingPageMenu);
    
    app.route('/inquirylandingPageHeader')
    .post(menuAdmin.inquirylandingPageHeader);

    app.route('/addLandingPageHeader')
    .post(menuAdmin.addLandingPageHeader);

    app.route('/updateLandingPageHeader')
    .post(menuAdmin.updateLandingPageHeader);

    app.route('/deleteLandingPageHeader')
    .post(menuAdmin.deleteLandingPageHeader);

    app.route('/inquirylandingPageContent')
    .post(menuAdmin.inquirylandingPageContent); 

    app.route('/addLandingPageContent')
    .post(menuAdmin.addLandingPageContent);

    app.route('/updateLandingPageContent')
    .post(menuAdmin.updateLandingPageContent);

    app.route('/deleteLandingPageContent')
    .post(menuAdmin.deleteLandingPageContent);

    app.route('/inquirylandingPageGroupContent')
    .post(menuAdmin.inquirylandingPageGroupContent); 

    app.route('/addLandingPageGroupContent')
    .post(menuAdmin.addLandingPageGroupContent);

    app.route('/updateLandingPageGroupContent')
    .post(menuAdmin.updateLandingPageGroupContent);

    app.route('/deleteLandingPageGroupContent')
    .post(menuAdmin.deleteLandingPageGroupContent);

    app.route('/inquiryMasterTypeTemplate')
    .post(menuAdmin.inquiryMasterTypeTemplate);    
    
    app.route('/addMasterTypeTemplate')
    .post(menuAdmin.addMasterTypeTemplate);

    app.route('/updateMasterTypeTemplate')
    .post(menuAdmin.updateMasterTypeTemplate);

    app.route('/deleteMasterTypeTemplate')
    .post(menuAdmin.deleteMasterTypeTemplate);

    app.route('/inquirylandingPageMenuByIdAdminAccs')
    .post(menuAdmin.inquirylandingPageMenuByIdAdminAccs);

    app.route('/inquirylandingPageHeaderByIdAdminAccs')
    .post(menuAdmin.inquirylandingPageHeaderByIdAdminAccs);

    app.route('/inquirylandingPageGroupContentByIdAdminAccs')
    .post(menuAdmin.inquirylandingPageGroupContentByIdAdminAccs);

    app.route('/inquirylandingPageContentByIdContent')
    .post(menuAdmin.inquirylandingPageContentByIdContent);
    //================== end route for admin ================== 
    
    //================== route for enduser ==================
    //login
    app.route('/loginEnduser')
    .post(loginEnduser.loginEnduser);

    app.route('/loginEnduserByGoogle')
    .post(loginEnduser.loginEnduserByGoogle);

    //userEnduser
    app.route('/registerEnduser')
    .post(userEnduser.registerEnduser);

    app.route('/inquiryEnduser')
    .post(userEnduser.inquiryEnduser);

    app.route('/selfUpdateEnduser')
    .post(userEnduser.selfUpdateEnduser);
    
    app.route('/forgotPasswordEnduser')
    .post(userEnduser.insertLupaPasswordEnduser);    
    
    //menu
    app.route('/inquiryMenu')
    .post(menuEnduser.inquiryMenu);

    app.route('/inquirySubMenuEnduser')
    .post(menuEnduser.inquirySubMenuEnduser);

    app.route('/addSubMenuEnduser')
    .post(menuEnduser.addSubMenuEnduser);

    app.route('/updateSubMenuEnduser')
    .post(menuEnduser.updateSubMenuEnduser);

    app.route('/deleteSubMenuEnduser')
    .post(menuEnduser.deleteSubMenuEnduser);
    
    app.route('/enduserAccessMenu')
    .post(menuEnduser.enduserAccessMenu);


    //uccpay
    app.route('/inquiryTrxHistoryUccPay')
    .post(uccpayEnduser.inquiryTrxHistoryUccPay);

    app.route('/inquiryUccPay')
    .post(uccpayEnduser.inquiryUccPay);
    //================== end route for enduser ==================

};
