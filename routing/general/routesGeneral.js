'use strict';

module.exports = function(app) {
    var general = require('../../atomic/general/general');
    var cache = require('../../atomic/general/cache');

    //route for general
    //common
    app.route('/')
    .get(general.index);
    
    app.route('/getKey')
    .get(general.getKey);

    app.route('/inquiryLanguage')
    .post(general.inquiryLanguage);

    //cache
    app.route('/initChace')
    .get(cache.initChace);
    
    app.route('/config')
    .get(cache.config);
    
};
