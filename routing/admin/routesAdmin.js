'use strict';

module.exports = function(app) {

    var loginAdmin = require('../../atomic/admin/login/loginAdmin');
    var menuAdmin = require('../../atomic/admin/menu/menuAdmin');
    var uccpayAdmin = require('../../atomic/admin/uccpay/uccpayAdmin');
    var userAdmin = require('../../atomic/admin/user/manageUserForAdmin');

    //route for admin 
    //loginAdmin   
    app.route('/loginAdmin')
    .post(loginAdmin.loginAdmin);

    app.route('/registerAdmin')
    .post(userAdmin.registerAdmin);


    //userAdmin
    app.route('/inquiryAdmin')
    .post(userAdmin.inquiryAdmin);

    app.route('/selfUpdateAdmin')
    .post(userAdmin.selfUpdateAdmin);

    app.route('/forgotPasswordAdmin')
    .post(userAdmin.insertLupaPasswordAdmin);
    
    //menuAdmin
    app.route('/masterAdminAccess')
    .post(menuAdmin.masterAdminAccess);

    app.route('/inquiryAdminMenu')
    .post(menuAdmin.inquiryAdminMenu);

    app.route('/addMenuAdmin')
    .post(menuAdmin.addMenuAdmin);

    app.route('/inquiryadminSubMenu')
    .post(menuAdmin.inquiryadminSubMenu);    

    app.route('/addSubMenuAdmin')
    .post(menuAdmin.addSubMenuAdmin);    

    app.route('/giveUserAccessToMenu')
    .post(menuAdmin.giveUserAccessToMenu);

    //admin manage enduser
    app.route('/addMenuEnduser')
    .post(menuAdmin.addMenuEnduser);
    
    app.route('/addSubMenuEnduser')
    .post(menuAdmin.addSubMenuEnduser);    


};
