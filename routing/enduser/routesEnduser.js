'use strict';

module.exports = function(app) {
    var loginEnduser = require('../../atomic/enduser/login/loginEnduser');
    var menuEnduser = require('../../atomic/enduser/menu/menu');
    var uccpayEnduser = require('../../atomic/enduser/uccpay/enduserUccPay');
    var userEnduser = require('../../atomic/enduser/user/enduserManage');    
    
    //route for enduser
    //login
    app.route('/loginEnduser')
    .post(loginEnduser.loginEnduser);

    app.route('/loginEnduserByGoogle')
    .post(loginEnduser.loginEnduserByGoogle);

    //userEnduser
    app.route('/registerEnduser')
    .post(userEnduser.registerEnduser);

    app.route('/inquiryEnduser')
    .post(userEnduser.inquiryEnduser);

    app.route('/selfUpdateEnduser')
    .post(userEnduser.selfUpdateEnduser);
    
    app.route('/forgotPasswordEnduser')
    .post(userEnduser.insertLupaPasswordEnduser);    
    
    //menu
    app.route('/inquiryMenu')
    .post(menuEnduser.inquiryMenu);

    app.route('/inquirySubMenuEnduser')
    .post(menuEnduser.inquirySubMenuEnduser);    
    
    app.route('/enduserAccessMenu')
    .post(menuEnduser.enduserAccessMenu);


    //uccpay
    app.route('/inquiryTrxHistoryUccPay')
    .post(uccpayEnduser.inquiryTrxHistoryUccPay);

    app.route('/inquiryUccPay')
    .post(uccpayEnduser.inquiryUccPay);

};
