const path = require('path');

module.exports = {
  entry: '../wrapper/server.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'api.bundle.js'
  },
  target: 'node'
};