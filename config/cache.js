const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

//future prod for redis
//var keyJwt = "Y2FtcHVzTWFuYWdlbWVudENvbnNvcnNpdW0=";
//var urlGoogleAuth = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=";
//var urlFaceBookAuth = "https://graph.facebook.com/me?access_token=";

const obj = { my: "keyJwtCache", variable: 127 };
const obj2 = { my: "urlFaceBookAuthCache", variable: 42 };
const obj3 = { my: "urlGoogleAuthCache", variable: 1337 };
 
const init = myCache.mset([
    {key: "urlFaceBookAuthCache", val: "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="},
    {key: "urlGoogleAuthCache", val: "https://graph.facebook.com/me?access_token="},
    {key: "keyJwtCache", val: "Y2FtcHVzTWFuYWdlbWVudENvbnNvcnNpdW0="},
])

var keyJwt = myCache.get( "keyJwtCache" );
var urlGoogleAuth = myCache.get( "urlGoogleAuthCache" );
var urlFaceBookAuth = myCache.get( "urlFaceBookAuthCache" );

function initChace() {
    const init = myCache.mset([
        {key: "urlFaceBookAuthCache", val: "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="},
        {key: "urlGoogleAuthCache", val: "https://graph.facebook.com/me?access_token="},
        {key: "keyJwtCache", val: "Y2FtcHVzTWFuYWdlbWVudENvbnNvcnNpdW0="},
    ])
}


module.exports = init;