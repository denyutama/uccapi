'use strict';

exports.ok = function(values, res) {
  var data = {
      'status': 200,
      'values': values
  };
  res.json(data);
  res.end();
};

exports.error = function(values, res) {
  var data = {
      'status': 500,
      'values': values
  };
  res.json(data);
  res.end();
};

exports.errorExternal = function(values, res) {
  var data = {
      'status': 999,
      'values': values
  };
  res.json(data);
  res.end();
};

exports.httpResp = function(values, code, res) {
  var data = {
      'status': code,
      'values': values
  };
  res.json(data);
  res.end();
};

exports.httpRespCus = function(content, values, code, res) {
  var data = {
      'status': code,
      'content': content,
      'values': values
  };
  res.json(data);
  res.end();
};

exports.httpRespWithInfo = function(values, data, code, res) {
  var data = {
      'status': code,
      'values': values,
      'data': data,
  };
  res.json(data);
  res.end();
};